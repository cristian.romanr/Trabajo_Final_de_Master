/*
 * Operations.h
 *
 *  Created on: 18 may 2023
 *      Author: Cristian Román Rodríguez
 */

#include <windows.h>
#include <iostream>
#include <vector>
using namespace std;
#ifndef OPERATIONS_H_
#define OPERATIONS_H_

class Operations {
	bool enc;
	string pathTemp;
	string obtenerPATH;

public:
	Operations();
	vector<char> leerBinario(string& filePath);
	void obtenerRutaArchivoAux(string directory, string filename);
	string ejecutarComandoCMD(string comando);
	string obtenerRutaActual();
	string crearArchivoMalicioso(string pathAtacar, string pathNewExe);
	string obtenerRutaArchivo(string directory, string filename);
	string obtenerPATHvulnerable();
	~Operations();
};

#endif /* OPERATIONS_H_ */
