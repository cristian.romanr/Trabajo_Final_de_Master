/*
 * PathInterception.h
 *
 *  Created on: 19 may 2023
 *      Author: Cristian Román Rodríguez
 */

#ifndef PATHINTERCEPTION_H_
#define PATHINTERCEPTION_H_

class PathInterception {
	Operations *operations;
	vector<string> exeFiles;
	vector<string> pathsVulnerables;
	vector<string> PATHs;
	string pathFile;
	string newExe;
	bool enc;

public:
	PathInterception(Operations *o);
	bool cambiarRutaExe(string nameExe);
	void eliminarPaths();
	void ejecutarExe(string filename);
	string obtenerPathVulnerable(string filename);
	string atacarPathVulnerable(string filename);
	string comprobarPath(string path);
	string buscarEjecutables(string path);
	string getLongPATH();
	string atacarPATH(string path);
	string obtenerExeVulnerable(string filename);
	string atackPathInterceptionHijacking(string filename);
	string crearArchivoMalicioso(string pathAtacar);
	~PathInterception();
};

#endif /* PATHINTERCEPTION_H_ */
