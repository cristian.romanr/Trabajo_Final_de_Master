/*
 * DllHijacking.h
 *
 *  Created on: 19 may 2023
 *      Author: Cristian Román Rodríguez
 */

#ifndef DLLHIJACKING_H_
#define DLLHIJACKING_H_

class DllHijacking {
	Operations *operations;
	string pathExe;
	string newDll;

public:
	DllHijacking(Operations *o);
	string obtenerDLLsVulnerables(string archivo, bool filtrar);
	string obtenerDllsUsadas(string filename);
	string filtrarDllsVulnerables(string entradaDlls, string pathfile);
	string atackDllSearchOrderHijacking(string filename);
	string obtenerDLLsSiofra(string filename);
	string atackDllSideLoading(string dllName);
	bool crearDllCombinada(string& dllPath1, string& dllPath2, string& resultPath);
	bool cambiarRutaDll(string nameDll);
	~DllHijacking();
};

#endif /* DLLHIJACKING_H_ */
