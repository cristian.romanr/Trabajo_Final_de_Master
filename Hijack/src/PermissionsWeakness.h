/*
 * PermissionsWeakness.h
 *
 *  Created on: 5 jun 2023
 *      Author: crist
 */

#ifndef PERMISSIONSWEAKNESS_H_
#define PERMISSIONSWEAKNESS_H_

class PermissionsWeakness {
	Operations *operations;
	vector<string> servicesName;
	string pathFile;
	string newExe;

public:
	PermissionsWeakness(Operations *o);
	void eliminarPaths();
	string atackServiceFile(string servicio, string path);
	string getPath(string service);
	string obtenerServiciosByType(string type);
	~PermissionsWeakness();
};

#endif /* PERMISSIONSWEAKNESS_H_ */
