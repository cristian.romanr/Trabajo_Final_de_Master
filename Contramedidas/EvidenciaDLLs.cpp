/*
 * Evidencia.cpp
 *
 *  Created on: 21 jun 2023
 *      Author: crist
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
#include <Wincrypt.h>
#include <Aclapi.h>
#include <cstring>
#include <regex>
#include <vector>
#include <unistd.h>
using namespace std;

#define BUFSIZE 1024
#define MD5LEN  16

/**
 * Método que calula el hash md5 de un determinado archivo y que lo devuelve como un string.
 * Este metodo sera usado para comprobar si una determianda dll, a traves de su hash, es legitima o no.
 *
 * @author Cristian Román Rodríguez
 */
string calcularMD5(LPCSTR dllPath) {
	string result="";
	bool bResult = false;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFSIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	char rgbDigits[] = "0123456789abcdef";
	hFile = CreateFile(dllPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (INVALID_HANDLE_VALUE == hFile) {
		return result;
	}

	// Get handle to the crypto provider
	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
		CloseHandle(hFile);
		return result;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return result;
	}

	while ((bResult = ReadFile(hFile, rgbFile, BUFSIZE, &cbRead, NULL))) {
		if (0 == cbRead) {
			break;
		}
		if (!CryptHashData(hHash, rgbFile, cbRead, 0)) {
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			CloseHandle(hFile);
			return result;
		}
	}

	if (!bResult) {
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		CloseHandle(hFile);
		return result;
	}

	cbHash = MD5LEN;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0)) {
		for (DWORD i = 0; i < cbHash; i++) {
			result = result + (rgbDigits[rgbHash[i] >> 4]) + (rgbDigits[rgbHash[i] & 0xf]);
		}
	}
	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);

	return result;
}

/**
 * Método que permite configurar los permisos adecuados para una DLL, restringiendo su uso
 * a determinados programas. Este metodo sera utiliado para evitar que otros programas u usarios
 * tengan acceso a dll que no deberian tener.
 *
 * @author Cristian Román Rodríguez
 */
bool setDLLPermissions(LPSTR dllPath){
    // Obtener el identificador de seguridad del archivo DLL
    PSECURITY_DESCRIPTOR pSD = nullptr;
    if (GetNamedSecurityInfoA(dllPath, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, nullptr, nullptr, &pSD) != ERROR_SUCCESS){
        return false;
    }

    // Crear una lista de control de acceso (ACL) con los permisos deseados
    EXPLICIT_ACCESSA explicitAccess;
    explicitAccess.grfAccessPermissions = GENERIC_READ; // Permisos de lectura
    explicitAccess.grfAccessMode = SET_ACCESS; // Establecer los permisos especificados
    explicitAccess.grfInheritance = NO_INHERITANCE; // No heredar los permisos a los subelementos
    explicitAccess.Trustee.TrusteeForm = TRUSTEE_IS_NAME;
    explicitAccess.Trustee.ptstrName = (LPSTR)"EvidenciaDLLs.exe";


    PACL pACL = nullptr;
    if (SetEntriesInAclA(1, &explicitAccess, nullptr, &pACL) != ERROR_SUCCESS){
        LocalFree(pSD);
        return false;
    }

    // Establecer los permisos en el archivo DLL
    HANDLE hFile = CreateFileA(dllPath, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
    if (hFile == INVALID_HANDLE_VALUE){
        LocalFree(pACL);
        LocalFree(pSD);
        return false;
    }
    if (SetSecurityInfo(hFile, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, nullptr, nullptr, pACL, nullptr) != ERROR_SUCCESS){
        CloseHandle(hFile);
        LocalFree(pACL);
        LocalFree(pSD);
        return false;
    }

    CloseHandle(hFile);
    LocalFree(pACL);
    LocalFree(pSD);
    return true;
}

/**
 * Método que comprueba si una determianda dll se encuentra en la lista blanca de dlls. Este metodo
 * sera utilizado para comprobar si una dll es legitima o no.
 *
 * @author Cristian Román Rodríguez
 */
bool isDllAllowed(string dllName) {
	string result="";
	char currentPath[128];
	getcwd(currentPath, sizeof(currentPath));
	string ruta = string(currentPath);
	replace(ruta.begin(), ruta.end(), '\\', '/');

	FILE *file = fopen((ruta +"/allowedDLLs.txt").c_str(), "r");
	if (file) {
		char buffer[256];
		while (fgets(buffer, sizeof(buffer), file) != nullptr) {
			result += buffer;
		}
		fclose(file);

		regex regex(";");
		sregex_token_iterator it(result.begin(), result.end(), regex, -1);
		sregex_token_iterator end;
		while (it != end) {
			if (dllName == *it) {
				return true;
			}
			++it;
		}
	}
    return false;
}
/*
 * El método main es el primero en ser ejecutado cuando se inicie el programa. Este metodo sera utlizado
 * para comprobar si la dll de la que hace uso es legitima o no.
 *
 * @author Cristian Román Rodríguez
 */
int main() {
	// Ruta absoluta de la DLL de la calculadora de Windows
	const char* dllPath = "C:\\Windows\\SysWOW64\\calculator.dll";

	//Se verifica la integridad de la ddl mediante el hash MD5.
	string hash = calcularMD5(dllPath);
	cout << "El hash md5 de la dll es: " + hash << endl;
	if (hash != "d9d84e7ba5dab06030f49849a5aed11d") {
		cout << "El hash de la dll cargada no corresponde con la dll legitima." << endl;
		return 1;
	}

	//Se configuran los permisos de la dll.
	if (!setDLLPermissions(strcpy(new char[strlen(dllPath) + 1], dllPath))) {
		cout << "Los permisos de la dll no han podido ser configurados correctamente."<< endl;
	}
	string path = dllPath;
	replace(path.begin(), path.end(), '\\', '/');
	if(!isDllAllowed(path.substr(path.find_last_of('/')+1, path.size()))){
		cout << "La dll no se encuentra en la lista blanca." << endl;
		return 1;
	}else{
		cout << "La dll se encuentra en la lista blanca." << endl;
	}

	// Carga de la DLL de la calculadora de Windows
	HMODULE calcDll = LoadLibraryA(dllPath);
	if (calcDll == nullptr) {
		cout << "No se pudo cargar la DLL de la calculadora." << endl;
		return 1;
	}

	// Obtención del puntero a la función de suma de la calculadora
	typedef int (__stdcall* AddFunc)(int, int);
	AddFunc add = reinterpret_cast<AddFunc>(GetProcAddress(calcDll, "Add"));
	if (add == nullptr) {
		cout << "No se pudo encontrar la funcion de suma en la DLL." << endl;
		return 1;
	}

	// Uso de la función de suma de la calculadora
	int result = add(2, 3);
	cout << "El resultado de la suma es: " << result << endl;

	// Descarga de la DLL
	FreeLibrary(calcDll);
	return 0;
}



