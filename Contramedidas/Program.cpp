/*
 * Program.cpp
 *
 *  Created on: 21 jun 2023
 *      Author: crist
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
#include <unistd.h>
using namespace std;

int main() {
    // Carga de la DLL de la calculadora de Windows
    HMODULE calcDll = LoadLibrary("calculator.dll");
    if (calcDll == nullptr) {
        cout << "No se pudo cargar la DLL de la calculadora." << endl;
        return 1;
    }

    // Obtención del puntero a la función de suma de la calculadora
    typedef int(__stdcall* AddFunc)(int, int);
    AddFunc add = reinterpret_cast<AddFunc>(GetProcAddress(calcDll, "Add"));
    if (add == nullptr) {
        cout << "No se pudo encontrar la función de suma en la DLL." << endl;
        return 1;
    }

    // Uso de la función de suma de la calculadora
    int result = add(2, 3);
    cout << "El resultado de la suma es: " << result << endl;

    // Descarga de la DLL
    FreeLibrary(calcDll);

    return 0;
}

int main(){
	system("cmd.exe /C calc users");

	sleep(300);
	return 0;
}
