/*
 * EvicenciaPath.cpp
 *
 *  Created on: 23 jun 2023
 *      Author: crist
 */

#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include <vector>
using namespace std;

/**
 * Método que sera será el encargado comprobar si dentro del binario hay un ruta vulnerable. Para ello
 * verifica que la ruta tenga un espacio en blanco, termine en exe y no empieze ni termine con comillas.
 * En el caso de encontrar una ruta vulnerable, la entrecomilla.
 *
 * @author Cristian Román Rodríguez
 */
void processFile(string filePath) {
	bool mod = false;
	string line;
	string tempFilePath = filePath.substr(0, filePath.find_last_of('.')) + "aux" + filePath.substr(filePath.find_last_of('.'));
	ifstream inputFile(filePath);
	ofstream tempFile(tempFilePath);
	while (getline(inputFile, line)) {
		//Se verifica que tenga un espacio en blanco, termine en exe y no empieze ni termine con comillas.
		if (line.find(' ') != string::npos && line.substr(line.size() - 4) == ".exe" && line.front() != '"' && line.back() != '"') {
			line = '"' + line + '"';
			mod = true;
		}
		tempFile << line << '\n';
	}
	inputFile.close();
	tempFile.close();

	// Sobrescribir el archivo original con el contenido modificado
	remove(filePath.c_str());
	rename(tempFilePath.c_str(), filePath.c_str());
	if (mod)
		cout << "Archivo modificado: " << filePath << endl;
}

/**
 * Método que sera será el encargado de recorrer todas las carpetas del sistema, buscando los ficheros
 * de dichas carpetas para comprobar si contienen rutas vulnerables.
 *
 * @author Cristian Román Rodríguez
 */
void traverseDirectories(string &directoryPath) {
	WIN32_FIND_DATA findData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	string searchPath = directoryPath + "\\*";

	hFind = FindFirstFile(searchPath.c_str(), &findData);
	if (hFind == INVALID_HANDLE_VALUE) {
		cout << "Directorio inválido: " << directoryPath << endl;
		return;
	}

	do {
		if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (string(findData.cFileName) != "." && string(findData.cFileName) != "..") {
				string subDirectoryPath = directoryPath + "\\" + findData.cFileName;
				traverseDirectories(subDirectoryPath); // Recursivamente, si se encuentran subdirectorios
			}
		} else {
			string filePath = directoryPath + "\\" + findData.cFileName;
			processFile(filePath);
		}
	} while (FindNextFile(hFind, &findData) != 0);

	FindClose(hFind);
}

/*
 * El método main es el primero en ser ejecutado cuando se inicie el programa. Este metodo sera utlizado
 * para comprobar que los binarios no tengan rutas con vulnerabilidades.
 *
 * @author Cristian Román Rodríguez
 */
int main(int argc, char* argv[]) {
	string path = "C:\\";
	if(argc >= 2){
		path = argv[1];
	}
	traverseDirectories(path);
	return 0;
}
